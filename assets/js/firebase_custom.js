$(document).ready(function(e){
    
   // Initialize Firebase
  var config = {
    apiKey: "AIzaSyCyZXiLUBHpAWvUhlDhlGcucX4pskYpzNs",
    authDomain: "msjapp-2e1d3.firebaseapp.com",
    databaseURL: "https://msjapp-2e1d3.firebaseio.com",
    projectId: "msjapp-2e1d3",
    storageBucket: "msjapp-2e1d3.appspot.com",
    messagingSenderId: "329086401422"
  };
  firebase.initializeApp(config);


    var 
      user_id               =   USER_ID,
      user_email            =   USER_EMAIL,
      receiver              =   RECEIVER,
      is_read               =   0,
      total_unread_count    =   0,
      chat_id               =   '',
      cps_data              =   [], // conversation partners data
      database              =   {},
      RootRef               =   firebase.database().ref('conversations'),
      chat_obj              =   {},
      prev_chat_ids         =   [];


    complete_message_data();
   
   //on press enter send message
    $('.message').keyup(function(e) {
        if (e.keyCode == 13) {
            SaveData($('.message').val());
            $('.message').val('');
          }
    });

    

    $('.SendMessage').click(function(){
        SaveData($('.message').val());
        $('.message').val('');
    });


     $(document).on('click', '.delete_message', function(e) {
        e.preventDefault();
        var unique_id = $(this).attr('data-attr');
        database.child(unique_id).remove();
    });

    $(document).on('click', '.show_friend_messages', function(e) {
        e.preventDefault();
        $('#chatDiv').modal('show');
        chat_id = $(this).attr('data-attr');
        $(this).addClass('active').siblings().removeClass('active');
        // set_request_parms(chat_id);

        // Detaches all callbacks
        detach_all_conversations();

        RootRef.child(chat_id).on('value' , function(snapshot){
            var obj = snapshot.val();
            ShowMessages(obj);
        });

        set_request_parms(chat_id);
        set_messages_read(chat_obj, chat_id);
    });

    $(document).on('click', '.closeChat', function(e) {
        e.preventDefault();
        var chatTextId = "QueuedMsg"+$(this).attr('id');
        $("."+chatTextId).html("Closed");
        $('#chatDiv').modal('hide');
        $(".startchat"+$(this).attr('id')).attr('disabled','disabled');
        $.ajax({
            url: base_url + 'cms/myChat/change_status_closed',
            type: 'post',
            dataType: 'json',
            data: {
                chat_request_id: $(this).attr('id')
            },
            success: function (res) {
                
            }
        });
    });

    

    function detach_all_conversations() {
        for (var i in chat_obj) {
            RootRef.child(i).off('value');
        }
    }
    
    function set_messages_read(chat, chatID) {
        if (chat) {
            chatID = chatID === undefined ? Object.keys(chat)[0] : chatID;
            // var chatID = Object.keys(chat)[0];

            for (var i in chat[chatID]) {
                // Update status of unread message
                firebase.database().ref('conversations/' + chatID).child(i).update({is_read : 1});
            }
        }
    }

    function show_notifications(chatID) {
        RootRef.child(chatID).orderByChild('receiver').equalTo(user_id).on('child_added' , function(data){
            
            if(Notification.permission !== 'default'){
                if(data.val().is_read === 0){
                    // var notify = new Notification(cps_data[receiver].email,{
                    var add_anchar = "<a href="+ base_url +'user/messages' + ">";
                    var notify = new Notification(cps_data[get_cp_user_id(chatID)].email,{
                        'body'  :   data.val().message,
                        'icon'  :   base_url +'assets_new/images/watermark_logo.png',
                        'tag'   :   data.getKey()
                    });
                    notify.onclick = function(event) {
                        location.href = base_url + 'user/messages';
                        notify.close();
                        //window.open('http://www.mozilla.org', '_blank');
                    }
                    /*notify.onclose = function(event) {
                        console.log(notify);
                    }*/
                }
            }else{
                Notification.requestPermission().then(function(p){});
            }
        });
    }

    function complete_message_data() {
        RootRef.on('value' , function(snapshot){
            var 
                obj = snapshot.val(),
                msg_keys_tmp = [],
                latestMsgOfConversation = {},
                chatIdsWithLatestMsgTime = {};  //  i.e: {14-16: "2017-6-1 14:29:22", 15-16: "2017-6-2 5:41:49", 16-45: "2017-6-6 13:27:56"}

            for(var i in obj) {
                // Conversation user ids
                var conv_user_ids = i.split('-');

                // If logged in user is one of conversation users
                if (conv_user_ids.indexOf(user_id) !== -1) {
                    chat_obj[i] = obj[i];

                    // console.log(obj[i]);
                    msg_keys_tmp = Object.keys(obj[i]);
                    // console.log(msg_keys_tmp);
                    latestMsgOfConversation = obj[i][msg_keys_tmp[msg_keys_tmp.length-1]];
                    // console.log(latestMsgOfConversation);
                    chatIdsWithLatestMsgTime[i] = latestMsgOfConversation.sending_date;
                }
            }

            chatIdsWithLatestMsgTime = sortChats(chatIdsWithLatestMsgTime);

            // Rearrange chat object as "chatIdsWithLatestMsgTime"
            chat_obj = sortChatsObject(chat_obj, chatIdsWithLatestMsgTime);

            ShowFriends(chat_obj);
            if (chat_obj){
                var keys = Object.keys(chat_obj);
                if(keys.length && receiver === 0){
                    // chat_id = keys[0];
                    chat_id = typeof requestedChatId === 'undefined' || requestedChatId === '' ? keys[0] : requestedChatId;
                    set_request_parms(chat_id);

                    RootRef.child(chat_id).on('value' , function(snapshot){
                        ShowMessages(snapshot.val());
                    });

                    // Set message as read, on page load
                    set_messages_read(chat_obj);
                }
            }
            
        });
        
    }
    
    function ShowMessages(obj){
        if (obj) {
            var html = '';
            var cp_id = 0;
            var cp_user_ids = [];
            var keys = Object.keys(obj);
            cp_id = obj[keys[0]].sender === user_id ? obj[keys[0]].receiver : obj[keys[0]].sender;
            cp_user_ids[0] = cp_id;
            cp_user_ids[1] = user_id;
            for (var i in obj) {
                if (obj[i].sender == user_id) {
                    html+= "<li class='clearfix'>";
                    html+= "<div class='chat-avatar'>";
                    html+= "<i>"+ getformatedDate(obj[i].sending_date) +"</i>";
                    html+= "</div><div class='conversation-text'><div class='ctext-wrap'>";
                    html+= "<p>"+ obj[i].message +"</p>";
                    html+= "</div></div></li>";
                }else{
                    html+= "<li class='clearfix odd'>";
                    html+= "<div class='chat-avatar'>";
                    html+= "<i>"+ getformatedDate(obj[i].sending_date) +"</i>";
                    html+= "</div><div class='conversation-text'><div class='ctext-wrap'>";
                    html+= "<p>"+ obj[i].message +"</p>";
                    html+= "</div></div></li>";

                    // Update status of unread message
                    // database.child(i).update({is_read : 1});
                }
            }

            // Set conversation partner id
            cp_id = obj[keys[0]].sender === user_id ? obj[keys[0]].receiver : obj[keys[0]].sender;

            $('.show_messages').html(html);

            var el = document.querySelectorAll('.show_messages');
            $(".messages-main-div, .show_messages").animate({ scrollTop: Object.keys(obj).length * 100});
            
            var cp_user_interval = setInterval(function() {
                if (cps_data.length && cps_data[cp_id] !== undefined) {
                    $('.show_username').html(cps_data[cp_id].username);
                    $('.show_username').next('span').children('button.closeChat').attr('id',cp_id);
                    clearInterval(cp_user_interval);
                }
            }, 100);
        }
    }

    function ShowFriends(obj){
        if (obj) {
            var 
                html = '',
                dropdown_html = '',
                cp_user_id ='',
                cp_user_ids = [];

            for (var i in obj) {
                cp_user_id = get_cp_user_id(i);
                cp_user_ids.push(cp_user_id);


            }

            $.ajax({
                url: base_url + 'cms/myChat/get_users_data',
                type: 'post',
                dataType: 'json',
                data: {
                    user_ids: cp_user_ids
                },
                success: function (res) {
                    set_users_data(res);

                    total_unread_count = 0;
                    var unread_count = 0;
                    // Loop through object to create conversations list
                    for (var i in obj) {

                        // Attach notification on new chats
                        if (prev_chat_ids.indexOf(i) === -1) {
                            show_notifications(i);
                        }

                        unread_count = 0;
                        // Loop through object of single conversation to get counter of unread messages
                        for (var j in obj[i])
                        {
                            var msg = obj[i][j];
                            if (msg.is_read == 0 && msg.receiver == user_id) {
                                unread_count++;
                            }
                        }

                        cp_user_id = get_cp_user_id(i);
                        //var email = cps_data[cp_user_id].email;
                        var username = cps_data[cp_user_id].username;
                        $('.startchat'+cp_user_id).attr('data-attr', i);
                        if($('.QueuedMsg'+cp_user_id).hasClass("NotClosed"))
                        {
                            $('.QueuedMsg'+cp_user_id).html(unread_count + " Queued Message");
                        }
                        html+="<a href='' class='show_friend_messages' data-attr="+ i +" >";
                        html+="<span class='user-circle'><i class='fa fa-user user-icon' aria-hidden='true'></i></span>"+ username;
                        html+="<span class='badge pull-right'>"+ unread_count +"</span>";
                        html+="</a>";

                        if (unread_count > 0) {
                            dropdown_html+="<li>";
                            dropdown_html+="<a href='" + base_url + "user/messages?chat_id=" + i + "' class='' data-attr="+ i +" >";
                            dropdown_html+=username;
                            dropdown_html+="<span class='badge pull-right'>"+ unread_count +"</span>";
                            dropdown_html+="</a>";
                            dropdown_html+="</li>";
                        }else{

                            dropdown_html ="<li>You have no message yet</li>";
                        }

                       total_unread_count += unread_count;
                    }
                    // Update "prev_chat_ids"
                    prev_chat_ids = Object.keys(chat_obj);
                    $('.show_friends').html(html);
                    $('.unread_msg_count').html(total_unread_count);
                    $('.msg-menu').html(dropdown_html);

                    $('button.show_friend_messages').removeClass('active')
                    $('button.show_friend_messages[data-attr="' + chat_id + '"]').addClass('active');
                }
            });
        }

    }

    function get_cp_user_id(c_id) {
        var cp_user_id = c_id.split('-');
        return cp_user_id[0] == user_id ? cp_user_id[1] : cp_user_id[0];
    }

    function set_request_parms(chat_id) {
        receiver = get_cp_user_id(chat_id);
        database = firebase.database().ref('conversations/' + chat_id);
    }

    function SaveData(message){
        if (message) {
        chat_id = user_id < receiver ? user_id + '-' + receiver : receiver + '-' + user_id;
        set_request_parms(chat_id);
          database.push().set({
            'sender': user_id,
            'receiver': receiver,
            'sender_email': user_email,
            'message': message,
            'is_read': is_read,
            'sending_date': ShowLocalDate(),
          });
        }
    }

    /**
     * @usage: It rearrange object of chat ids
     * @arg (chatIdsWithLatestMsgTime) (object): 
     *  Structure should be like this: {14-16: "2017-6-1 14:29:22", 15-16: "2017-6-2 5:41:49", 16-45: "2017-6-6 13:27:56"}
     *  Afer sort it will rearranged as: {16-45: "2017-6-6 13:27:56", 15-16: "2017-6-2 5:41:49", 14-16: "2017-6-1 14:29:22"}
     */
    function sortChats(chatIdsWithLatestMsgTime) {
        var sortable = [];
        for (var i in chatIdsWithLatestMsgTime) {
            sortable.push([i, chatIdsWithLatestMsgTime[i]]);
        }

        sortable.sort(function(a, b) {
            return new Date(a[1]) > new Date(b[1]) ? -1 : 1;
        });

        chatIdsWithLatestMsgTime = {};
        for(var i=0; i<sortable.length; i++) {
            chatIdsWithLatestMsgTime[sortable[i][0]] = sortable[i][1];
        }

        return chatIdsWithLatestMsgTime;
    }

    function sortChatsObject(obj, chatIdsWithLatestMsgTime) {
        var tmp_obj = {};

        for(var i in chatIdsWithLatestMsgTime) {
            tmp_obj[i] = obj[i];
        }

        return tmp_obj;
    }

    function get_email_by_id(users, user_id){
        for (var i = users.length - 1; i >= 0; i--) {
            if (users[i].id == user_id) {
                return users[i].email;
            }
        }
    }

    function set_users_data(users) {
        for (var i = users.length - 1; i >= 0; i--) {
            cps_data[users[i].id] = {
                email: users[i].email,
                username: users[i].username
            };
        }
    }
    function ShowLocalDate(){
        var dNow = new Date();
        return localdate= dNow.getFullYear()  + '-' + (dNow.getMonth()+1) + '-' + dNow.getDate() + ' ' + dNow.getHours() + ':' + dNow.getMinutes() + ':' + dNow.getSeconds();
    }


    function getformatedDate (dateString = new Date()) {
        var monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];
        var monthNamesPrefix = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul",
            "Aug", "Sep", "Oc",
            "Nov", "Dec"
        ];

        var 
            d = new Date(dateString),
            year = d.getFullYear(),
            month = monthNamesPrefix[d.getMonth()],
            date = d.getDate(),
            hours = d.getHours(),
            minutes = d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes(),
            am_pm = 'AM';

        if (hours >= 12) {
            hours -= 12;
            am_pm = 'PM';
        }
        if (hours == 0) {
            hours = 12;
        } else if (hours < 10) {
            hours = '0' + hours;
        }
        dateString = month + ' ' + date + ', ' + year + ' | ';
        dateString += hours + ':' + minutes + ' ' + am_pm;
  
        return dateString;
    }

    var email_user_id = {};
    /*Autocomplete Email Start*/
    var cache = {};
    var url = base_url + 'user/autocomplete';
    /*$('.new_msg_form input[name="new_user_name"]').autocomplete({
        minLength: 2,
        source: function( request, response ) {
            var term = request.term;
            if ( term in cache ) {
                response( cache[ term ] );
                return;
            }
            $.getJSON(url, request, function( data, status, xhr ) {
                cache[ term ] = data;
                response( data );

                for(i in data){
                    email_user_id[data[i].id] = data[i].value;
                }
            });
        }
    });*/

    /*Autocomplete Email End*/


    $(document).on('submit', '.new_msg_form', function(e) {
        e.preventDefault();
        var 
            form = $(this),
            new_user_name = form.find('[name="new_user_name"]').val(),
            new_message = form.find('[name="new_message"]').val();

            if (new_user_name && new_message) {

                $.each(email_user_id, function(key,valueObj){

                    if (valueObj == new_user_name) {
                       receiver = key;
                    }
                });

                chat_id = user_id < receiver ? user_id + '-' + receiver : receiver + '-' + user_id;
                    set_request_parms(chat_id);
                    database.push().set({
                        'sender': user_id,
                        'receiver': receiver,
                        'sender_email': user_email,
                        'message': new_message,
                        'is_read': is_read,
                        'sending_date': ShowLocalDate(),
                      });
                    setTimeout(function() {
                        $('#new_message_modal').modal('hide');
                    }, 1000);


                // Detaches all callbacks
                detach_all_conversations();

                RootRef.child(chat_id).on('value' , function(snapshot){
                    ShowMessages(snapshot.val());
                });

                $('#email').val('');
                $('#new_message').val('');
            }
           
        
    });

    /*setInterval(function() {
        console.log('chat_id: ' + chat_id);
        console.log('user_id' + user_id);
        console.log('receiver' + receiver);
    }, 2000);*/
});