$( document ).ready(function() {
var unsaved = false;
$(".form_data").submit(function(e){
	
		
		
		e.preventDefault();
		$form = $(this);
    
    
    
       $.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } });
    
    
		
		$.ajax({
				type: "POST",
				url: $form.attr('action'),
				data: new FormData( this ),
				dataType:"json",
				cache: false,
				contentType: false,
   			    processData: false,
				//async:false,
				success: function(result){
					
					/*if(result.close == 'true')
					{
						document.getElementById("closeModel").click();
					}
					*/
                $.unblockUI;    
					if(result.error != 'false'){
                        
                        if($('#validatio-msg').hasClass('alert-success'))
                        {
                                $('#validatio-msg').removeClass('alert-success');
                        }
                        if(!$('#validatio-msg').hasClass('alert-danger'))
                        {
                                $('#validatio-msg').addClass('alert-danger');
                        }
						
						$("#validatio-msg").html(result.error);
						$('#validatio-msg').show();
						
					}else{
						if($('#validatio-msg').hasClass('alert-danger'))
                        {
                                $('#validatio-msg').removeClass('alert-danger');
                        }
                        if(!$('#validatio-msg').hasClass('alert-success'))
                        {
                                $('#validatio-msg').addClass('alert-success');
                        }
                        
						$("#validatio-msg").html(result.success);
						$('#validatio-msg').show();
						if(result.reset)
							$form[0].reset();
						if(result.reload)
						setTimeout(function(){ window.location.reload(); }, 1000);
						
					if(result.redirect)
					{
						setTimeout(function(){ window.location.href = base_url+result.url; }, 1000);
						
					}
						document.getElementById("show_success_messge").click();
						
					}
					
					
				
				},complete: function(){
                $.unblockUI();  
            }
		});
	});
	
	
	
	
	
	
		
			

		// datatable
	/*	
	$('.dynamic-table-user thead th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
    } );
 
    // DataTable
	if ($('.dynamic-table-user').length > 0) {
    var table = $('.dynamic-table-user').DataTable({
			     "ordering": false
			   
	    });

 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.header() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
	}
		
		
		if ($('#dynamic-table').length > 0) {
		$('#dynamic-table').DataTable({
			     "ordering": false
			   
		});
		}
		
		if ($('#admin-job-table').length > 0) {
		$('#admin-job-table').DataTable({
			     "ordering": false,
				  "paging":   false,
				  "searching": false,
			   
		});
		}
		if ($('.dynamic-table').length > 0) {
		$('.dynamic-table').DataTable({
			     "ordering": false
			   
			   });
		}
		//end datatable	*/
		//image preview
	/*
	
	var email_to_admin_check = 0;
	 $( ".email_to_admin" ).on( "click", function() {
			
			 if(email_to_admin_check == 0)
			 {
				 
				var email = $("#createJobEmail").val();
				var telephone = $("#createJobTelephone").val();
			
				var job_budget = $("#JobBudget").val();
				var job_title = $("#createJobTitle").val();
				if(email != '')
				{
					 var postData = $('.job_add_form').serialize();
					$.ajax({
						type: "POST",
						url: base_url+'home/informToAdmin',
						data: postData,
						dataType:"json",
						cache: false,
						//async:false,
						success: function(result){
							if(result.sent == 1)
							{
								 email_to_admin_check++;
							}
						}
				   });
				   email_to_admin_check++;
					
				}
				
			 }
  	
	
				
	});
	*/
		
	
	if($('.geocomplete_map_draggable').length > 0){
        $(".geocomplete_map_draggable").geocomplete({
            details: ".GeoDetails",
            detailsAttribute: "data-geo",
            map: ".address_map",
            location: [49.2827,123.1207],
            mapOptions: {
                zoom: 9
            },
            markerOptions: {
                draggable: true
            }
        });
        // draggable change lat long
        $(".geocomplete_map_draggable").bind("geocode:dragged", function(event, latLng){
            $("input[name=latitude]").val(latLng.lat());
            $("input[name=longitude]").val(latLng.lng());
            var lat = latLng.lat();
            var lng = latLng.lng();

            $.ajax({
                url: 'http://maps.googleapis.com/maps/api/geocode/json',
                type: 'GET',
                data: {
                    'latlng': lat+','+lng,
                    'sensor': 'true'
                },
                success: function (data) {
                	//console.log('Location: ', data['results']);
                        
                    var address = data['results'][0].formatted_address;
                    $("input[name='address']").val(address);
                   // $("input[name='city']").val(data['results'][0].address_components[2].long_name);
                   //  $("input[name='country']").val(data['results'][0].address_components[4].long_name);
                  
                }

            });

        });
    }	
	
	
});



function deleteRecord(id,actionUrl,reloadUrl)
{

	//id can contain comma separated ids too.
	
	if(confirm("Are you sure you want to delete this?"))
	{
		$.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } });
        
        $.ajax({
				type: "POST",
				url: base_url+''+actionUrl,
				data: {'id' : id, 'form_type' : 'delete'},
				dataType:"json",
				cache: false,
				//async:false,
				success: function(result){
				 
			if(result.error != 'false'){
				if($('#validatio-msg').hasClass('alert-success'))
                {
                        $('#validatio-msg').removeClass('alert-success');
                }
                if(!$('#validatio-msg').hasClass('alert-danger'))
                {
                        $('#validatio-msg').addClass('alert-danger');
                }

                $("#validatio-msg").html(result.error);
                $('#validatio-msg').show();
				
			}else{
				$('#'+id).remove();
				if($('#validatio-msg').hasClass('alert-danger'))
                {
                        $('#validatio-msg').removeClass('alert-danger');
                }
                if(!$('#validatio-msg').hasClass('alert-success'))
                {
                        $('#validatio-msg').addClass('alert-success');
                }

                $("#validatio-msg").html(result.success);
                $('#validatio-msg').show();
				if(reloadUrl!="") setTimeout(function(){document.location.href = reloadUrl;},1000);
				}
				
			},complete: function(){
                $.unblockUI();  
            }
		});
		return true;
	} else {
		return false;
	}
	
}


function deleteSpefication(id)
{

	//id can contain comma separated ids too.
	
	if(confirm("Are you sure you want to delete this?"))
	{
		$.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } });
        
        $.ajax({
				type: "POST",
				url: base_url+'cms/product/action',
				data: {'id' : id, 'form_type' : 'delete_specification'},
				dataType:"json",
				cache: false,
				//async:false,
				success: function(result){
				 
			if(result.error != 'false'){
				alert('There is something went wrong');
				
			}else{
				$('#spec-'+id).remove();
				alert('Deleted Successfully');
				}
				
			},complete: function(){
                $.unblockUI();  
            }
		});
		return true;
	} else {
		return false;
	}
	
}

/*function deleteRecord(id,actionUrl,reloadUrl)
{

	//id can contain comma separated ids too.
	
	if(confirm("Are you sure you want to delete?"))
	{
		$.ajax({
				type: "POST",
				url: base_url+''+actionUrl,
				data: {'id' : id, 'form_type' : 'delete'},
				dataType:"json",
				cache: false,
				//async:false,
				success: function(result){
				
			if(result.error != 'false'){
				$("#mySmallModalLabel").html('Fehler');
				$("#message").html(result.error);
				document.getElementById("show_success_messge").click();
				
			}else{
				$('#'+id).remove();
				$("#mySmallModalLabel").html('Erfolg');
				$("#message").html(result.success);
				document.getElementById("show_success_messge").click();
				if(reloadUrl!="") document.location.href = reloadUrl;
				}
				
			}
		});
		return true;
	} else {
		return false;
	}
	
}*/

function deleteImage(id,actionUrl)
{

	//id can contain comma separated ids too.
	
	if(confirm("Are you sure you want to delete?"))
	{
		$.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } });
        
        $.ajax({
				type: "POST",
				url: base_url+''+actionUrl,
				data: {'id' : id, 'form_type' : 'delete_image'},
				dataType:"json",
				cache: false,
				//async:false,
				success: function(result){
                    
            
			if(result.error != 'false'){
				alert('There is something went wrong');
				
			}else{
				$('#img-'+id).remove();
				alert('Deleted Successfully');
				}
                    
				
			},complete: function(){
                $.unblockUI();  
            }
		});
		return true;
	} else {
		return false;
	}
	
}



function redirect(redirect_to){
	redirect_to = base_url+redirect_to;
    window.location.href = redirect_to;	
}


