$(document).ready(function () {

    $(document).on('click', '.map_modal', function () {
        var lat = $(this).attr('data-lat');
        var lng = $(this).attr('data-lng');

        $(".map_with_pin").geocomplete({
            map: ".map_with_pin",
            location: [lat,lng],
            mapOptions: {
                zoom: 16
            }
        });
    });
    


});