-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jun 13, 2019 at 08:13 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `sunny_n_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `block_users`
--

CREATE TABLE `block_users` (
  `block_id` int(11) NOT NULL,
  `blocked_user_id` int(11) NOT NULL,
  `blocked_by_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `block_users`
--

INSERT INTO `block_users` (`block_id`, `blocked_user_id`, `blocked_by_user_id`) VALUES
(1, 10, 12);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `category_image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `title`, `color`, `description`, `category_image`, `created_at`) VALUES
(1, 'Food', '#FA556B', '', '', '2018-11-23 15:05:58'),
(2, 'Nightlife+Pubs', '#000000', '', '', '2018-11-23 15:05:58'),
(3, 'Clothing', '#E6D24B', '', '', '2018-11-23 15:06:37'),
(4, 'Beauty+Spas', '#3CE6BE', '', '', '2018-11-23 15:06:37'),
(5, 'Health+Fitness', '#78D24B', '', '', '2018-11-23 15:10:51'),
(6, 'Travel', '#4691DC', '', '', '2018-11-23 15:10:51'),
(7, 'Automotive', '#8C5632', '', '', '2018-11-23 15:12:52'),
(8, 'Fun To Do', '#00C4EE', '', '', '2018-11-23 15:12:52'),
(9, 'Makeup', '#D72BE5', '', '', '2018-11-23 15:13:32'),
(10, 'Classes+Courses', '#442BCA', '', '', '2018-11-23 15:13:32'),
(11, 'Restaurant', '#F75C2A', '', '', '2018-11-23 15:14:06'),
(12, 'Medical', '#D50021', '', '', '2018-11-23 15:14:06');

-- --------------------------------------------------------

--
-- Table structure for table `chat_groups`
--

CREATE TABLE `chat_groups` (
  `chat_group_id` int(11) NOT NULL,
  `group_id` varchar(255) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chat_groups`
--

INSERT INTO `chat_groups` (`chat_group_id`, `group_id`, `sender_id`, `receiver_id`, `status_id`, `created_at`) VALUES
(1, '2', 11, 12, 4, '2018-07-12 15:12:04'),
(3, '4', 12, 11, 3, '2018-07-12 15:11:57'),
(4, '5', 12, 10, 3, '2018-07-12 15:11:57');

-- --------------------------------------------------------

--
-- Table structure for table `chat_message_count`
--

CREATE TABLE `chat_message_count` (
  `chat_message_count_id` int(11) NOT NULL,
  `chat_group_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message_id` varchar(255) NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chat_message_count`
--

INSERT INTO `chat_message_count` (`chat_message_count_id`, `chat_group_id`, `user_id`, `message_id`, `count`) VALUES
(1, '2', 0, '1', 2),
(2, '2', 0, '2', 2),
(3, '2', 0, '3', 2),
(4, '4', 0, '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `report_id` int(11) NOT NULL,
  `report_type` enum('','user','status') DEFAULT NULL,
  `user_or_status_id` int(11) NOT NULL,
  `reported_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `report`
--

INSERT INTO `report` (`report_id`, `report_type`, `user_or_status_id`, `reported_by`) VALUES
(1, 'user', 1, 2),
(2, 'status', 45, 17),
(3, 'status', 46, 17),
(4, 'status', 47, 17),
(5, 'status', 48, 7),
(6, 'status', 52, 2147483647),
(7, 'status', 52, 7),
(8, 'status', 53, 18),
(9, 'status', 46, 17),
(10, 'status', 78, 17),
(11, 'status', 73, 2147483647),
(12, 'status', 73, 18),
(13, 'status', 55, 18);

-- --------------------------------------------------------

--
-- Table structure for table `trip_images`
--

CREATE TABLE `trip_images` (
  `trip_image_id` int(11) NOT NULL,
  `trip_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trip_members`
--

CREATE TABLE `trip_members` (
  `trip_member_id` int(11) NOT NULL,
  `trip_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `gender` enum('','Female','Male') NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `device_token` varchar(500) NOT NULL,
  `device_type` varchar(255) NOT NULL,
  `image` varchar(500) NOT NULL,
  `address` varchar(500) CHARACTER SET utf8 NOT NULL,
  `user_latitude` varchar(255) NOT NULL,
  `user_longitude` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `distance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `username`, `category_id`, `gender`, `phone`, `email`, `password`, `device_token`, `device_type`, `image`, `address`, `user_latitude`, `user_longitude`, `created_at`, `updated_at`, `distance`) VALUES
(1, 'Sarfraz', '', 1, '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', '', 'djfadsjlfjasdlfjkadslkfj', '', '', '2018-11-09 19:51:30', '0000-00-00 00:00:00', 0),
(2, 'Sarfraz Update', '', 2, '', '', 'sarfraz.cs10@gmail.com', 'aa8fc0f314347b21d81eadbba9f38504', '', '', 'uploads/images/9023420181109075355checkout3 safraz-002.jpg', 'djfadsjlfjasdlfjkadslkfj', '70', '78', '2018-11-09 19:52:30', '0000-00-00 00:00:00', 0),
(3, 'chacha feeka', '', 7, '', 'Victory Container Movies, Wadala West, Wadala, Mumbai, Maharashtra 400031, India', 'test@asd.asd', '202cb962ac59075b964b07152d234b70', '', '', '', 'Victory Container Movies, Wadala West, Wadala, Mumbai, Maharashtra 400031, India', '19.0176147', '72.8561644', '2018-11-23 22:08:12', '0000-00-00 00:00:00', 0),
(4, 'chacha feeka', '', 7, '', 'Anand Medical & General Stores, 2/206, Citizen Society, Cadell Road, Next To Hinduja Hospital, Mahim (W), Swatantrya Veer Savarkar Marg, Mahim West, Mahim, Mumbai, Maharashtra 400016, India', 'test@test.test', '2bd7c83d7860679fcfd1761983c67ee9', '', '', '', 'Anand Medical & General Stores, 2/206, Citizen Society, Cadell Road, Next To Hinduja Hospital, Mahim (W), Swatantrya Veer Savarkar Marg, Mahim West, Mahim, Mumbai, Maharashtra 400016, India', '19.0176147', '72.8561644', '2018-11-23 22:13:58', '0000-00-00 00:00:00', 0),
(5, 'chacha feeka', '', 8, '', '20/19 Road Number 19, Wadala West, Mumbai, Maharashtra, 400031, India', 'test1@test.test', '202cb962ac59075b964b07152d234b70', '', '', '', '20/19 Road Number 19, Wadala West, Mumbai, Maharashtra, 400031, India', '0', '0', '2018-11-23 22:17:34', '0000-00-00 00:00:00', 0),
(6, 'chacha feeka', '', 2, '', 'Stonehenge, Amesbury, Salisbury SP4 7DE, UK', 'test2@test.test', '202cb962ac59075b964b07152d234b70', '', '', '', 'Stonehenge, Amesbury, Salisbury SP4 7DE, UK', '51.17888199999999', '-1.826215', '2018-11-24 19:29:30', '0000-00-00 00:00:00', 0),
(7, 'chacha feeka', '', 2, '', 'Stonehenge, Amesbury, Salisbury SP4 7DE, UK', 'test3@test.test', '202cb962ac59075b964b07152d234b70', '', '', '', 'Stonehenge, Amesbury, Salisbury SP4 7DE, UK', '51.17888199999999', '-1.826215', '2018-11-24 19:45:02', '0000-00-00 00:00:00', 0),
(8, 'chacha feeka', '', 2, '', 'Stonehenge, Amesbury, Salisbury SP4 7DE, UK', 'test4@test.test', '202cb962ac59075b964b07152d234b70', '', '', '', 'Stonehenge, Amesbury, Salisbury SP4 7DE, UK', '51.17888199999999', '-1.826215', '2018-11-24 19:49:41', '0000-00-00 00:00:00', 0),
(9, 'chacha feeka', '', 1, '', 'Stonehenge, Amesbury, Salisbury SP4 7DE, UK', 'test5@test.test', '202cb962ac59075b964b07152d234b70', '', '', '', 'Stonehenge, Amesbury, Salisbury SP4 7DE, UK', '51.17888199999999', '-1.826215', '2018-11-24 22:37:16', '0000-00-00 00:00:00', 0),
(10, 'chacha feeka', '', 1, '', '9 Wardour St, 9 Wardour St, London W1D 6PF, UK', 'test6@test.test', '202cb962ac59075b964b07152d234b70', '', '', '', '9 Wardour St, 9 Wardour St, London W1D 6PF, UK', '51.5108058', '-0.1318716', '2018-11-24 22:55:20', '0000-00-00 00:00:00', 0),
(11, 'chacha feeka', '', 11, '', 'The Criterion Theatre, 218-223 Piccadilly, St. James\'s, London W1V 9LB, UK', 'test7@test.test', '3d40d121450c0b7ee8b7c07d2f0b2f1d', '', '', '', 'The Criterion Theatre, 218-223 Piccadilly, St. James\'s, London W1V 9LB, UK', '51.5097608', '-0.1343267', '2018-11-24 23:21:33', '0000-00-00 00:00:00', 0),
(12, 'chacha feeka', '', 4, '', 'Libertad 150, Libertad 150, Peralvillo, Morelos, 06200 Morelos, CDMX, Mexico', 'test8@test.test', '202cb962ac59075b964b07152d234b70', '', '', '', 'Libertad 150, Libertad 150, Peralvillo, Morelos, 06200 Morelos, CDMX, Mexico', '19.4441369', '-99.13217880000001', '2018-11-25 00:11:12', '0000-00-00 00:00:00', 0),
(13, 'chacha feeka123', '', 3, '', 'Chins Dance Academy, Parvati Niwas, Matunga CR. East, Matunga, Mumbai, Maharashtra 400019, India', 'test9@test.test', '202cb962ac59075b964b07152d234b70', '', '', '', 'Chins Dance Academy, Parvati Niwas, Matunga CR. East, Matunga, Mumbai, Maharashtra 400019, India', '19.023618', '72.85705299999999', '2018-11-26 11:44:10', '0000-00-00 00:00:00', 0),
(14, 'chacha feeka theka', '', 7, '', '1122', 'test10@test.test', '202cb962ac59075b964b07152d234b70', '', '', '', 'Gulf of Oman, Gulf of Oman', '24.7142573', '58.73736339999999', '2018-11-26 18:55:46', '0000-00-00 00:00:00', 0),
(15, 'chacha feeka', '', 2, '', '1122', 'test11@test.test', '202cb962ac59075b964b07152d234b70', '', '', '', 'Victory Container Movies, Wadala West, Wadala, Mumbai, Maharashtra 400031, India', '19.0176147', '72.8561644', '2018-11-28 15:49:36', '0000-00-00 00:00:00', 0),
(16, 'chacha feeka123 chacha feeka', '', 1, '', '+911122334455', 'test12@test.test', '202cb962ac59075b964b07152d234b70', '', '', '', 'Victory Container Movies, Wadala West, Wadala, Mumbai, Maharashtra 400031, India', '19.0176147', '72.8561644', '2018-11-28 18:16:00', '0000-00-00 00:00:00', 0),
(17, 'feeka chacha feeka', '', 1, '', '1122', 'test13@test.test', '202cb962ac59075b964b07152d234b70', '', '', '', 'SBM Toilet, Ram nagar, Wadla Village, Wadala, Mumbai, Maharashtra 400001, India', '19.0169233', '72.8560141', '2018-11-28 18:54:11', '0000-00-00 00:00:00', 0),
(18, 'Pizza Hut', '', 1, '', '+16044013400 ', 'sun_14@live.ca', '85b6c99bb36d6e7be78bf8fd28d6e43d', '', '', '', 'Pizza Hut, 8155 Granville St, Vancouver, BC V6P 4Z6, Canada', '49.2121552', '-123.1409273', '2018-11-29 13:38:55', '0000-00-00 00:00:00', 0),
(19, 'chacha feeka', '', 8, '', '1122', 'sunnymalhi17@gmail.com', '778609db5dc7e1a8315717a9cdd8fd6f', '', '', '', 'Starbucks, 8002 Granville St, Vancouver, BC V6P 4Z4, Canada', '49.2130533', '-123.1401448', '2018-11-30 21:21:00', '0000-00-00 00:00:00', 0),
(20, 'iphone', '', 8, '', '+9203481234567', 'mobile_test@test.test', '25d55ad283aa400af464c76d713c07ad', '', '', '', 'Lahore, Lahore, Punjab, Pakistan', '0', '0', '2018-12-02 10:30:21', '0000-00-00 00:00:00', 0),
(21, 'gateway pizza ', '', 1, '', '+16044013400 ', 'j.singh@hotmail.com', '202cb962ac59075b964b07152d234b70', '', '', '', 'Gateway Pizza & Pasta, 9183 148 St, Surrey, BC V3R 3W7, Canada', '49.16990790000001', '-122.812614', '2018-12-03 02:09:22', '0000-00-00 00:00:00', 0),
(22, 'iphone 1', '', 1, '', '0678464', 'mobile_test1@test.test', '202cb962ac59075b964b07152d234b70', '', '', '', 'Book Mart, Ghazi Rd, Khuda Buksh Colony Gulshan e Ali Colony, Lahore, Punjab, Pakistan', '31.492751', '74.4171492', '2018-12-03 09:30:26', '0000-00-00 00:00:00', 0),
(23, 'churchs chicken', '', 1, '', '+16044013400 ', 'r.singh@hotmail.com', '202cb962ac59075b964b07152d234b70', '', '', '', 'Church\'s Chicken, 6591 Kingsway #103, Burnaby, BC V5E 1E1, Canada', '49.219635', '-122.9680301', '2018-12-03 17:38:57', '0000-00-00 00:00:00', 0),
(24, 'surrey Punjab ', '', 11, '', '+16044013400 ', 'rhea.malhi@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 'Surrey Punjab Dhaba, 12830 96 Ave Unit 3, Surrey, BC V3V 6A8, Canada', '49.1761461', '-122.8671013', '2018-12-09 19:49:28', '0000-00-00 00:00:00', 0),
(25, 'Nature', '', 8, '', '012635798', 'hamza.farooq60@hotmail.com', '202cb962ac59075b964b07152d234b70', '', '', '', 'Piccadilly Circus, Mayfair, London, England, W1J, United Kingdom', '0', '0', '2018-12-12 06:32:13', '0000-00-00 00:00:00', 0),
(26, 'Nature', '', 8, '', '123345123', 'hamza_faroooq@yahoo.com', '202cb962ac59075b964b07152d234b70', '', '', '', 'Victory Container Movies, Wadala West, Wadala, Mumbai, Maharashtra 400031, India', '19.0176147', '72.8561644', '2018-12-12 06:38:03', '0000-00-00 00:00:00', 0),
(27, 'BiggRed', '', 5, '', '510-555-1212', 'flyflyerson@gmail.com', '17c38e75f2313407f6d4e42240d27e61', '', '', '', 'Apple Campus, Infinite Loop, Cupertino, CA 95014, USA', '37.3320003', '-122.0307812', '2018-12-14 18:02:39', '0000-00-00 00:00:00', 0),
(28, 'hmmm ', '', 9, '', '+923323911622 ', 'un@b.com', '006d2143154327a64d86a264aea225f3', '', '', '', 'Ghousia Colony, Lahore, Punjab, Pakistan', '0', '0', '2018-12-15 09:12:29', '0000-00-00 00:00:00', 0),
(29, 'tommy gun', '', 4, '', '+16044013400 ', '123@hotmail.com', '2c347b362363dc267e1b5c9ffe1a878f', '', '', '', 'Tommy Gun\'s Original Barbershop, 10355 152 St Unit 2550, Surrey, BC V3R 7C1, Canada', '49.1909474', '-122.8036771', '2018-12-17 02:52:43', '0000-00-00 00:00:00', 0),
(30, 'vancity bakery', '', 1, '', '+16044013400 ', '234@hotmail.com', '2c347b362363dc267e1b5c9ffe1a878f', '', '', '', 'The Bakery, 1670 Franklin St, Vancouver, BC V5L 1P4, Canada', '49.28209589999999', '-123.0709845', '2018-12-17 02:54:36', '0000-00-00 00:00:00', 0),
(31, 'The Juice Truck', '', 1, '', '+16044013400 ', '456@hotmail.com', '2c347b362363dc267e1b5c9ffe1a878f', '', '', '', 'The Juice Truck 5th Ave, 28 W 5th Ave, Vancouver, BC V5Y 1H5, Canada', '49.2663543', '-123.1054578', '2018-12-17 02:56:51', '0000-00-00 00:00:00', 0),
(32, 'Cactus Club', '', 11, '', '+16044013400 ', '789@hotmail.com', '2c347b362363dc267e1b5c9ffe1a878f', '', '', '', 'Cactus Club Cafe, 7907 120 St, Delta, BC V4C 6N6, Canada', '49.14691179999999', '-122.8911391', '2018-12-17 02:59:08', '0000-00-00 00:00:00', 0),
(33, 'Fortune Sound Club', '', 2, '', '+16044013400 ', '543@hotmail.com', '2c347b362363dc267e1b5c9ffe1a878f', '', '', '', 'Fortune Sound Club, 147 E Pender St, Vancouver, BC V6A 1T5, Canada', '49.2806632', '-123.1008', '2018-12-17 03:02:59', '0000-00-00 00:00:00', 0),
(34, 'The Henry Public House ', '', 2, '', '+16044013400 ', '890@hotmail.com', '2c347b362363dc267e1b5c9ffe1a878f', '', '', '', 'The Henry Public House, 5708 176 St, Surrey, BC V3S 4E3, Canada', '49.10626250000001', '-122.7346763', '2018-12-17 03:06:15', '0000-00-00 00:00:00', 0),
(35, 'Moxie’s Grill & Bar', '', 11, '', '+16044013400 ', '657@hotmail.com', '2c347b362363dc267e1b5c9ffe1a878f', '', '', '', 'Moxie\'s Grill & Bar, 10608 151 A St, Surrey, BC V3R 1J8, Canada', '49.1951471', '-122.8014107', '2018-12-17 03:10:36', '0000-00-00 00:00:00', 0),
(36, 'Burger', '', 1, '', '+923323911622 ', 'salman@gmail.con', '25d55ad283aa400af464c76d713c07ad', '', '', '', 'Jeddah, Jeddah Saudi Arabia', '21.485811', '39.19250479999999', '2018-12-18 19:56:13', '0000-00-00 00:00:00', 0),
(37, 'SAStore', '', 2, '', '+92 348 6558008 ', 'satest@test.test', '202cb962ac59075b964b07152d234b70', '', '', '', 'Jeddah, Jeddah Saudi Arabia', '21.485811', '39.19250479999999', '2018-12-21 07:57:50', '0000-00-00 00:00:00', 0),
(38, 'satest1@test.test', '', 2, '', '+92 348 6558008 ', 'satest1@test.test', '202cb962ac59075b964b07152d234b70', '', '', '', '7013 Ahl at Tawadi, 7013 Ahl at Tawadi, As Sahifah District, Jeddah 22237 3799, Saudi Arabia', '21.485828', '39.1930619', '2018-12-21 08:12:08', '0000-00-00 00:00:00', 0),
(39, 'fly', '', 3, '', '852369852', 'fly@gmail.com', '75a593a34aa5ba8e5e5788b7c899802e', '', '', '', '95134, San Jose, CA 95134, USA', '37.4308503', '-121.9529992', '2018-12-22 10:50:08', '0000-00-00 00:00:00', 0),
(40, 'bill', '', 5, '', '12365', 'bill@gmail.com', 'e8375d7cd983efcbf956da5937050ffc', '', '', '', '01531050, 01531050, Alviso, CA 95134, USA', '37.4336672', '-121.9316672', '2018-12-22 10:53:49', '0000-00-00 00:00:00', 0),
(41, 'fly', '', 7, '', '23568', 'bill123@gmail.com', '1f3870be274f6c49b3e31a0c6728957f', '', '', '', 'HomElegance By Topline, 47550 Kato Rd, Fremont, CA 94538, USA', '37.4729383', '-121.9302278', '2018-12-22 11:21:06', '0000-00-00 00:00:00', 0),
(42, 'tommy gun barber', '', 4, '', '+16044013400 ', 'bsmalhi@hotmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 'Tommy Gun\'s Original Barbershop, 10355 152 St Unit 2550, Surrey, BC V3R 7C1, Canada', '49.1909474', '-122.8036771', '2019-01-01 02:20:09', '0000-00-00 00:00:00', 0),
(43, 'The Cleaning Fairty', '', 4, '', '+18327272856 ', 'joinermyia@icloud.com', 'c9aa0ac162dff3cdc9cbc9249f6d3a26', '', '', '', 'Friendswood Lake Homeowners, 1644 Mossy Stone Dr, Friendswood, TX 77546, USA', '29.491043', '-95.201722', '2019-01-10 21:34:07', '0000-00-00 00:00:00', 0),
(44, 'The Juice Truck', '', 1, '', '7783250410', 's.singh@hotmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 'The Juice Truck, 4236 Main St, Vancouver, BC V5V 2H1, Canada', '49.2472075', '-123.1009425', '2019-01-14 06:02:32', '0000-00-00 00:00:00', 0),
(45, 'Sushi Oyama', '', 11, '', '6042581012', 'v.123@hotmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 'Sushi Oyama, 5152 Kingsway, Burnaby, BC V5H 2E8, Canada', '49.22503469999999', '-122.9899912', '2019-01-14 06:08:17', '0000-00-00 00:00:00', 0),
(46, 'ABC', '', 12, '', '+17783250410 ', 'bluerover17@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', '12676 96a Ave, 12676 96a Ave, Surrey, BC V3V 2B4, Canada', '49.1778831', '-122.8714017', '2019-01-15 04:55:25', '0000-00-00 00:00:00', 0),
(47, 'Uncle fatih’s Pizza', '', 1, '', '+16044013400 ', 'r.123@hotmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 'Uncle Fatih\'s Pizza, 1685 E Broadway, Vancouver, BC V5N 1V9, Canada', '49.262487', '-123.070132', '2019-01-23 15:19:48', '0000-00-00 00:00:00', 0),
(48, 'KoKo', '', 9, '', '7787980901', 'sejukanan@gmail.com', '0914dd9b0bca884264b1756252c7c7df', '', '', '', '6959 Fraser St, 6959 Fraser St, Vancouver, BC V5X 3V3, Canada', '49.2212778', '-123.0911586', '2019-01-24 19:35:51', '0000-00-00 00:00:00', 0),
(49, 'ABC', '', 1, '', '+16044013400 ', 'abc@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', '12676 96a Ave, 12676 96a Ave, Surrey, BC V3V 2B4, Canada', '49.1778831', '-122.8714017', '2019-01-31 02:52:15', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_trips`
--

CREATE TABLE `user_trips` (
  `user_trip_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `block_users`
--
ALTER TABLE `block_users`
  ADD PRIMARY KEY (`block_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `chat_groups`
--
ALTER TABLE `chat_groups`
  ADD PRIMARY KEY (`chat_group_id`);

--
-- Indexes for table `chat_message_count`
--
ALTER TABLE `chat_message_count`
  ADD PRIMARY KEY (`chat_message_count_id`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`report_id`);

--
-- Indexes for table `trip_images`
--
ALTER TABLE `trip_images`
  ADD PRIMARY KEY (`trip_image_id`);

--
-- Indexes for table `trip_members`
--
ALTER TABLE `trip_members`
  ADD PRIMARY KEY (`trip_member_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_trips`
--
ALTER TABLE `user_trips`
  ADD PRIMARY KEY (`user_trip_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `block_users`
--
ALTER TABLE `block_users`
  MODIFY `block_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `chat_groups`
--
ALTER TABLE `chat_groups`
  MODIFY `chat_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `chat_message_count`
--
ALTER TABLE `chat_message_count`
  MODIFY `chat_message_count_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `report_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `trip_images`
--
ALTER TABLE `trip_images`
  MODIFY `trip_image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trip_members`
--
ALTER TABLE `trip_members`
  MODIFY `trip_member_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `user_trips`
--
ALTER TABLE `user_trips`
  MODIFY `user_trip_id` int(11) NOT NULL AUTO_INCREMENT;
