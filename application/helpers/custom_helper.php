<?php
if (!defined('BASEPATH'))
 exit('No direct script access allowed');


    function currentDate()
	{
		return date('Y-m-d H:i:s');
	}

	function checkKeyForThisUser($data = array())
	{
	   
	   $CI = & get_Instance();
	   $CI->load->model('Model_key');
	   
	   $result = $CI->Model_key->getWithMultipleFields($data);
	   
	   if($result){
		   return true;
	   }else{
		   
		   return false;
	   }
	}
	
	function uploadFileFromBase64($base64encoded_string, $path, $extension = 'png')
	{
	    $filename = rand(9999, 99999999999) . date('Ymdhsi');
	    $upload_path = $path . $filename . "." . $extension;
	    file_put_contents($upload_path, base64_decode($base64encoded_string));
	    return $upload_path;
	}
	
	function checkVoteToImage($user_id,$image_id)
	{
	   
	   $CI = & get_Instance();
	   $CI->load->model('Model_feed_action');
	   
	   $fetch_by['feed_image_id'] = $image_id;
	   $fetch_by['user_id'] 	  = $user_id;
	   $fetch_by['type'] 	      = 'vote';
	   $result = $CI->Model_feed_action->getWithMultipleFields($fetch_by);
	   
	   if($result){
		   return $result;
	   }else{
		   
		   return false;
	   }
	}
	
	function checkTotalVoteToImage($image_id,$singal_image_vote = false,$value = 1)
	{
	   
	   $CI = & get_Instance();
	   $CI->load->model('Model_feed_action');
	   
	   $result = $CI->Model_feed_action->checkTotalVoteToImage($image_id,$singal_image_vote,$value);
	   
	   return $result;
	}
	
	
	function genrateKey()
	{
	   return md5('candy'.date('mYdsiH').date('siHdmY').'ballin');
	   
	}


	function RandomString()
	{
	    $characters = '0123456789012345678901234567890123456789012345678912';
	    $randstring = '';
	    for ($i = 0; $i < 4; $i++) {
	        $randstring .= $characters[rand(0, 50)];
	    }
	    return $randstring;
	}
    
	
   function checkAdminSession()
   {
	   $CI = & get_Instance();
	   if($CI->session->userdata('admin'))
	   {
		   return true;
		   
	   }else
	   {
		   redirect($CI->config->item('base_url'));
	   }
   }
   
   
   function sendEmail($data= array())
	{
	
	
		$CI = & get_Instance();
		$CI->load->library('email');
		$CI->email->from($data['from']);
		$CI->email->to($data['to']);
		$CI->email->subject($data['subject']);
		$CI->email->message($data['body']);
		$CI->email->set_mailtype('html');
		

		
		
		
		
		
     
		
		
		
		if($CI->email->send()){
			//echo 'yes';exit();
				return true;

		}else
		{
			// show_error($CI->email->print_debugger());
			//echo 'no';exit();
			return false;
		}
		
		/*$CI = & get_Instance();

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= "From: " .'<'.$data['from'].'>'. "\r\n";  
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
     	$headers .= "Bcc: '.$emailList->Email.'\r\n";
		
		if (mail($data['to'], $data['subject'], $data['body'], $headers)) {
			//echo 'yes';
				return true;

		}else
		{
			// show_error($CI->email->print_debugger());
			//echo 'no';exit();
			return false;
		}*/

	}
 
 
	