<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjob extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 
	 */
	 function __construct()

    	{

        // Construct the parent class

        parent::__construct();



        
        $this->load->model('Model_status');
        $this->load->model('Model_status_view');


    	}
	public function index()
	{
		
            $delete_before = date('Y-m-d H:i:s', strtotime('-1 days'));
            $status = $this->Model_status->getStatus($delete_before);
            
            if(!empty($status)){
                $deleted_by = array();
                foreach($status as $value){
                    unlink($value['status_image']);
                    $deleted_by['user_status_id'] = $value['user_status_id'];
                    $this->Model_status_view->delete($deleted_by);
                }
                $this->Model_status->deletePreviousStatus($delete_before);  
            }
            
            
	}
	
}
