<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Job extends CI_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
		parent::__construct();
		checkAdminSession();
       
        $this->load->model('Model_job');
        $this->load->model('Model_category');
       // $this->load->model('Model_category_text');
		
	}
	 
    
    public function index()
	{
		
        $this->data['view'] = 'backend/job/manage';
        
        $this->data['jobs'] = $this->Model_job->getListing($empty = array());
        $this->load->view('backend/layouts/default',$this->data);
	}
    public function add()
    {

        $this->data['view'] = 'backend/job/add';
        $fetch_by = array();
        $fetch_by['is_active'] = 1;
        $this->data['categories'] = $this->Model_category->getMultipleRows($fetch_by);
        $this->load->view('backend/layouts/default',$this->data);
    }
    
    public function edit($job_id)
	{
        
        $this->data['result']		        = $this->Model_job->get($job_id,false,'job_id');
	
        if(!$this->data['result']){
           redirect(base_url('cms/job')); 
        }
        
        $fetch_by = array();
        $fetch_by['is_active'] = 1;
        $this->data['categories'] = $this->Model_category->getMultipleRows($fetch_by);
        $this->data['view'] = 'backend/job/edit';
        $this->data['job_id'] 	 = $job_id;
	$this->load->view('backend/layouts/default',$this->data);
		
	}
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->validate();
                $this->save();
          break; 
            case 'update':
                $this->validate();
                $this->update();
          break;
            case 'delete':
                //$this->validate();
                $this->delete();
          break; 
                 
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('category_id', 'Category', 'required');



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
	{
		$post_data = $this->input->post();
	
		
		
		$save_data['title']                     = $post_data['title'];
		$save_data['category_id']               = $post_data['category_id'];
		$save_data['city']                      = $post_data['city'];
		$save_data['country']                   = $post_data['country'];
		$save_data['address']                   = $post_data['address'];
		
		$save_data['price']                     = $post_data['price'];
                $save_data['shift_type']                  = $post_data['shift_type'];
		$save_data['description']               = $post_data['description'];
		$save_data['timestamp']                 = strtotime(date('Y-m-d H:i:s'));
                
                $save_data['job_type']                = $post_data['job_type'];
                $save_data['latitude']                  = $post_data['latitude'];
                $save_data['longitude']                 = $post_data['longitude'];
                
                $save_data['user_id']                   = -1;
        
            
		$save_data['image'] = $this->uploadImage("image", "uploads/images/");
		
		$save_data['created_at']          = date('Y-m-d H:i:s');		
		
		
        
		$insert_id = $this->Model_job->save($save_data);
		if($insert_id > 0)
		{
			
            
            
            
			$success['error']   = 'false';
			$success['success'] = 'Save Successfully';
			$success['redirect'] = true;
			$success['url'] = 'cms/job';
			echo json_encode($success);
			exit;
			
			
		}else
		{
			$errors['error'] = 'There is something went wrong';
			$errors['success'] = 'false';
			echo json_encode($errors);
			exit;
		}
	}
    
        private function update()
	{
		$post_data = $this->input->post();
	
		
		
		$save_data['title']                     = $post_data['title'];
		$save_data['category_id']               = $post_data['category_id'];
		$save_data['city']                      = $post_data['city'];
		$save_data['country']                   = $post_data['country'];
		$save_data['address']                   = $post_data['address'];
		
		$save_data['price']                     = $post_data['price'];
                $save_data['shift_type']                = $post_data['shift_type'];
		$save_data['description']               = $post_data['description'];
		$save_data['timestamp']                 = strtotime(date('Y-m-d H:i:s'));
                
                $save_data['job_type']                = $post_data['job_type'];
                $save_data['latitude']                  = $post_data['latitude'];
                $save_data['longitude']                 = $post_data['longitude'];
                
                $save_data['user_id']                   = -1;
        
                if(isset($_FILES['image']["name"][0]) && $_FILES['image']["name"][0] != ''){
                     $save_data['image'] = $this->uploadImage("image", "uploads/images/");
                 }
		
		
		$save_data['created_at']          = date('Y-m-d H:i:s');
		
              
            $update_by = array();
            $update_by['job_id'] = $post_data['job_id'];
	    $this->Model_job->update($save_data,$update_by);
		
        


        $success['error']   = 'false';
        $success['success'] = 'Updated Successfully';
        $success['redirect'] = true;
        $success['url'] = 'cms/job';
        echo json_encode($success);
        exit;
	}
    
    private function uploadImage($file_key, $path,$id=false,$multiple = false)
    {


       
		  $data = array();
		 $extension=array("jpeg","jpg","png","gif");
		 foreach($_FILES[$file_key]["tmp_name"] as $key=>$tmp_name)
            {
                $file_name= rand(9999,99999999999).date('Ymdhsi').str_replace(' ','_',$_FILES[$file_key]['name'][$key]);
                $file_tmp=$_FILES[$file_key]["tmp_name"][$key];
                $ext=pathinfo($file_name,PATHINFO_EXTENSION);
                if(in_array($ext,$extension))
                {
                       
                        move_uploaded_file($file_tmp=$_FILES[$file_key]["tmp_name"][$key], $path.$file_name);
                        if(!$multiple){
                            return $path.$file_name;
                        }else{
                            
                            
                        }
                        /*$data['DestinationID'] = $id; 
					    $data['ImagePath'] = $path.$file_name; 
					    $this->Model_destination_image->save($data);*/
                    
                }
               
            }
			return true;


    }
    
    
   
    
    private function delete(){
        
        $get_data = $this->Model_job->get($this->input->post('id'),false,'job_id');
        if(file_exists($get_data->image)) {
              unlink($get_data->image);
        }
      
        $deleted_by = array();
        $deleted_by['job_id'] = $this->input->post('id');
        $this->Model_job->delete($deleted_by);
        $success['error']   = 'false';
        $success['success'] = 'Deleted Successfully';
        
        echo json_encode($success);
        exit;
    }
	

}