<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
		parent::__construct();
		checkAdminSession();
       
        $this->load->model('Model_user');
       // $this->load->model('Model_category_text');
		
	}
	 
    
    public function index()
	{
		
        $this->data['view'] = 'backend/user/manage';
        
        $this->data['users'] = $this->Model_user->getAll();
        $this->load->view('backend/layouts/default',$this->data);
	}
    
	

}