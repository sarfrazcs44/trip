<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('Model_user');
		
		
	}
	 
	public function index()
	{
          if($this->session->userdata('admin'))
           {
              redirect(base_url('cms/category'));

           }
        
		  redirect(base_url('cms/account/login'));
	}

	//User Login
	public function login() 
	{
		if($this->session->userdata('admin'))
           {
              redirect(base_url('cms/category'));

           }
		$this->data['view'] = 'backend/login';
        $this->load->view('backend/login',$this->data);
		
	}
    
    
    public function checkLogin(){
		$data = array();
		$post_data = $this->input->post();
		$this->loginValidation();
		$checkUser = $this->checkUser($post_data);
		
		if($checkUser != true)
		{
			$data = array();
			$data['success'] = 'false';
			$data['error'] = 'Email or password incorrect.';
			echo json_encode($data);
			exit();
		}else
		{
			$data = array();
			$data['success'] = 'Login Successfully';
			$data['error'] = 'false';
            $data['redirect'] = true;
            $data['url'] = 'cms/category';
			echo json_encode($data);
			exit();
		}
	}
		
    private function loginValidation()
	{
		    

			$errors = array();
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
           
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			
			
			
			if ($this->form_validation->run() == FALSE)
			{
				$errors['error'] = validation_errors();
				$errors['success'] = 'false';
				echo json_encode($errors);
				exit;
			}else
			{
				return true;
			}
			
	}
	private function checkUser($post_data)
	{
		//$post_data['password'] = md5($post_data['password']);
            $post_data['id'] = -1; // this is admin id
            $user = $this->Model_user->getWithMultipleFields($post_data);
	    
		if(!empty($user)){
			
			/*if($user->isVerified != '1')
			{
				$data = array();
				$data['success'] = 'false';
				$data['error'] = lang('you_account_is_not_verified');
				echo json_encode($data);
				exit();
				
			}*/
			$user = (array)$user;
			$this->session->set_userdata('admin',$user);
			//$this->updateUserLoginStatus();
			return true;
		}else{
			return false;
		}
		
	}
	
	

	//Logout
	public function logout()
	{
		$data = array();
		$arr_update = array();
		$user = $this->session->userdata('admin');
		//$arr_update['id'] = $user['id'];
		//$this->Model_user->update($data,$arr_update);
		$this->session->unset_userdata('admin');
		 /*unset($_SESSION['userdata']);
		 unset($_SESSION['loggedin_user_id']);
 		 unset($_SESSION['user']);
		 unset($_SESSION['FBID']);
    	 unset($_SESSION['FULLNAME']);
    	 unset($_SESSION['EMAIL']); 
		 unset($_SESSION['FBRLH_state']);
		 session_destroy();*/
		redirect($this->config->item('base_url').'cms/account/login');
		
	}
}