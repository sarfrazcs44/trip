<?php


defined('BASEPATH') OR exit('No direct script access allowed');


// This can be removed if you use __autoload() in config.php OR use Modular Extensions

require APPPATH . '/libraries/REST_Controller.php';

date_default_timezone_set('UTC');


/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Api extends REST_Controller
{


    function __construct()

    {

        // Construct the parent class

        parent::__construct();


        // Configure limits on our controller methods

        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php

        $this->methods['user_get']['limit'] = 50000; // 500 requests per hour per user/key

        $this->methods['user_post']['limit'] = 10000; // 100 requests per hour per user/key

        $this->methods['user_delete']['limit'] = 5000; // 50 requests per hour per user/key

        $this->load->model('Model_user');
        //$this->load->model('Model_dialog');
       // $this->load->model('Model_status');
       // $this->load->model('Model_status_view');
       // $this->load->model('Model_chat_group'); 
       // $this->load->model('Model_chat_message_count'); 
        $this->load->model('Model_block_user'); 
        $this->load->model('Model_report'); 
        $this->load->model('Model_category');
        $this->load->model('Model_user_trip');
        $this->load->model('Model_user_trip_image');
        $this->load->model('Model_trip_member');
        $this->load->model('Model_trip_comment');
        $this->load->model('Model_notification');


    }


    public function userRegistration_post()

    {

        $data = array();

        $data_fetch = array();

        $post_data = $this->input->post();


        if (!empty($post_data)) {


            $user = false;
            $user2 = false;

            if (isset($post_data['email'])) {
                $data_fetch['email'] = $post_data['email'];
                $user = $this->Model_user->getMultipleRows($data_fetch, true);
            }
            
            if ($user) {


                $this->response([

                    'status' => FALSE,

                    'message' => 'Email already exist'

                ], REST_Controller::HTTP_OK);


            } else {


                $user_data = array();
                foreach ($post_data as $key => $value) {
                    $user_data[$key] = $value;

                }



                $user_data['created_at'] = date('Y-m-d H:i:s');
                if(isset($user_data['password'])){
                    $user_data['password'] = md5($user_data['password']);
                }
                $file_name = '';
                if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {

                    $file_name = $this->uploadImage("image", "uploads/images/");
                    $file_name = "uploads/images/" . $file_name;
                }
                $user_data['image'] = $file_name;
                $insert_id = $this->Model_user->save($user_data);

                $user = $this->Model_user->getUserData('users.user_id = '.$insert_id);


                $this->response([

                    'status' => TRUE,

                    'user_info' => $user

                ], REST_Controller::HTTP_OK);


            }

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }

   

    /*public function categories_get()
    {
        $categories = $this->Model_category->getAll(true);
        $post_data  =  $this->input->get();
         if (!empty($post_data)) {
            $post_data['distance'] = 30; 

            $return_array = array();
            if($categories){

                foreach ($categories as $key => $value) {
                    $post_data['category_id'] = $value['category_id'];
                    $status = array();
                    $status = $this->Model_status->getNearestStatus($post_data);
                    $value['status'] = $status;

                    $return_array[] = $value;
                   
                }
            }
            $this->response([
                'status' => TRUE,
                'categories' => $return_array
            ], REST_Controller::HTTP_OK);
         }else{
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        

    }*/

    public function searchUsers_post()
    {

        $data = array();
        $post_data = $this->input->post();


        if (!empty($post_data)) {


            $users = $this->Model_user->searchUsers($post_data['name']);

            $this->response([

                'status' => TRUE,

                'user_info' => $users

            ], REST_Controller::HTTP_OK);


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function login_post()

    {

        $data = array();

        

        $post_data = $this->input->post();

        if (!empty($post_data)) {


             


                
                $user = $this->Model_user->getUserData("users.email = '".$post_data['email']."' AND users.password = '".md5($post_data['password'])."' ");

                if ($user) {


                   


                    $this->response([

                        'status' => TRUE,

                        'user_info' => $user

                    ], REST_Controller::HTTP_OK);


                } else {

                    $this->response([

                        'status' => FALSE,

                        'message' => 'Email or password incorrect'

                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                }
            
        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }

    public function uploadImage($key, $path)

    {


        $file_name = '';

        $file_name = rand(999, 999999) . date('Ymdhsi') . $_FILES[$key]['name'];

        $file_size = $_FILES[$key]['size'];

        $file_tmp = $_FILES[$key]['tmp_name'];

        $file_type = $_FILES[$key]['type'];

        move_uploaded_file($file_tmp, $path . $file_name);

        return $file_name;


    }


    public function uploadMultipleImages($key, $path, $i)

    {


        $file_name = '';

        $file_name = date('Ymdhsi') . $_FILES[$key]['name'][$i];

        $file_size = $_FILES[$key]['size'][$i];

        $file_tmp = $_FILES[$key]['tmp_name'][$i];

        $file_type = $_FILES[$key]['type'][$i];

        move_uploaded_file($file_tmp, $path . $file_name);

        return $file_name;


    }



    public function forgotPasswordForFirebase_post()
    {
        $data = $this->input->post();
        $email = array();
        if ($data) {
                $new_password = $data['password'];
                
                $email['body'] = 'Your current (new) password is ' . $new_password;
                $email['to'] = $data['email'];
                $email['from'] = 'no-reply@bts.com';
                $email['subject'] = 'Forgot Password';
                sendEmail($email);
                 $this->response([
                'status' => TRUE,
                'message' => 'Your password is sent to your email'
                ], REST_Controller::HTTP_OK);
        } else {

            $this->response([
                'status' => FALSE,
                'message' => 'No data found'
            ], REST_Controller::HTTP_OK); 

        }

    }

    

    public function forgotPassword_post()
    {
        $data = $this->input->post();
        if ($data) {
            $fetch_by = array();
            $fetch_by['email'] = $data['email'];
            $user = $this->Model_user->getWithMultipleFields($fetch_by);
            if ($user) {

                $new_password = RandomString();
                $update = array();
                $update['password'] = md5($new_password);
               
                $this->Model_user->update($update,$fetch_by);
                $email['body'] = 'Your current (new) password is ' . $new_password;
                $email['to'] = $user->email;
                $email['from'] = 'no-reply@trip.com';
                $email['subject'] = 'Forgot Password';
                $mail_sent = sendEmail($email);
                if ($mail_sent) {
                    $this->response([
                        'status' => TRUE,
                        'message' => 'Your password is sent at your email address'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No user found with this email'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {

            $this->response([
                'status' => FALSE,
                'message' => 'No data found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }

    }


    

    public function updateUser_post()
    {
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            $update_by['user_id'] = $post_data['user_id'];
            unset($post_data['user_id']);
            if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
                $file_name = $this->uploadImage("image", "uploads/images/");
                $file_name = "uploads/images/" . $file_name;
                $post_data['image'] = $file_name;
            }

            if(isset($post_data['password'])){
                    $post_data['password'] = md5($post_data['password']);
            }
            $this->Model_user->update($post_data, $update_by);
            $user = $this->Model_user->getUserData("users.user_id = ".$update_by['user_id']);
            $this->response([
                'status' => TRUE,
                'user_info' => $user
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
    
    
    public function saveTrip_post()
    {
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            
            $file_name = '';
            $save_user_trip = array();
            $save_user_trip['title'] = $post_data['title'];
            $save_user_trip['user_id'] = $post_data['user_id'];
            $save_user_trip['created_at'] = $post_data['created_at'];
            $insert_id = $this->Model_user_trip->save($save_user_trip);
            if($insert_id > 0)
            {

                

                

                if(isset($post_data['trip_members'])){
                    $users = explode(',',$post_data['trip_members']);
                    foreach ($users as $key => $user) {
                        $save_trip_members = array();
                        $save_trip_members['user_id'] = $user;
                        $save_trip_members['trip_id'] = $insert_id;
                        $this->Model_trip_member->save($save_trip_members);
                    }


                    $notification_save = array();
                    $notification_save['user_id']       =  $post_data['user_id'];
                    $notification_save['trip_id']       =  $insert_id;
                    $total = Count($users);
                    $total = $total - 1;
                    $notification_save['notification']  =  '[user] created an album with you & '.$total.' others "[trip]"';
                    $notification_save['created_at']    =  time();
                    $this->Model_notification->save($notification_save);
                }
	        //$status_data = $this->Model_status->getStatusData('user_status.user_status_id = '.$insert_id);  
                // Uploading Product Images
                ini_set('memory_limit', '-1');
                $product_images_count = (isset($post_data['image_count']) && $post_data['image_count'] > 0 ? $post_data['image_count'] : 0);
                for ($i = 1; $i <= $product_images_count; $i++) {
                    $product_image_data = array();
                    $filename = date('Ymdhsi') . rand(9999, 99999999999);
                    $product_image_data['trip_id'] = $insert_id;
                    $product_image_data['image'] = uploadFileFromBase64($post_data['image' . $i], "uploads/images/");
                    
                    $this->Model_user_trip_image->save($product_image_data);
                }


                $trip_data = $this->Model_user_trip->get($insert_id,true,'user_trip_id');
                $trip_data['images'] = array();
                $trip_images = $this->Model_user_trip_image->getMultipleRows(array('trip_id' => $insert_id),true);
                if($trip_images){
                    $trip_data['images'] = $trip_images;
                }
                $trip_data['members'] = $this->Model_user->getUsers($insert_id);

                $this->response([
	                'status' => TRUE,
	                'trip_detail' => $trip_data
	            ], REST_Controller::HTTP_OK);
            }else{
            	
                $this->response([
                 'status' => FALSE,
                 'message' => 'There is something went wrong'
                ], REST_Controller::HTTP_OK);
            }
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function updateTrip_post()
    {
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            
            $file_name = '';
            $save_user_trip = array();
            $save_user_trip_by = array();
            $save_user_trip['title'] = $post_data['title'];
            //$save_user_trip['user_id'] = $post_data['user_id'];
            $save_user_trip_by['user_trip_id'] = $post_data['user_trip_id'];
           // $save_user_trip['created_at'] = $post_data['created_at'];
            $this->Model_user_trip->update($save_user_trip,$save_user_trip_by);
             $insert_id = $post_data['user_trip_id'];
            if(isset($post_data['trip_members'])){
                $deleted_by = array();
                $deleted_by['trip_id'] = $post_data['user_trip_id'];
                $this->Model_trip_member->delete($deleted_by);
                    $users = explode(',',$post_data['trip_members']);
                    foreach ($users as $key => $user) {
                        $save_trip_members = array();
                        $save_trip_members['user_id'] = $user;
                        $save_trip_members['trip_id'] = $insert_id;
                        $this->Model_trip_member->save($save_trip_members);
                    }
                }


            //$status_data = $this->Model_status->getStatusData('user_status.user_status_id = '.$insert_id);  
                // Uploading Product Images
                ini_set('memory_limit', '-1');
                $product_images_count = (isset($post_data['image_count']) && $post_data['image_count'] > 0 ? $post_data['image_count'] : 0);
                for ($i = 1; $i <= $product_images_count; $i++) {
                    $product_image_data = array();
                    $filename = date('Ymdhsi') . rand(9999, 99999999999);
                    $product_image_data['trip_id'] = $insert_id;
                    $product_image_data['user_id'] = $post_data['user_id'];
                    $product_image_data['image'] = uploadFileFromBase64($post_data['image' . $i], "uploads/images/");
                    
                    $this->Model_user_trip_image->save($product_image_data);
                }

                if($product_images_count > 0){
                    $notification_save = array();
                    $notification_save['user_id'] =  $post_data['user_id'];
                    $notification_save['trip_id'] =  $insert_id;
                    $notification_save['notification'] =  '[user] added '.$product_images_count.' photos in [trip]';
                    $notification_save['created_at'] =  time();
                    $this->Model_notification->save($notification_save);

                }


                $trip_data = $this->Model_user_trip->get($insert_id,true,'user_trip_id');
                $trip_data['images'] = array();
                $trip_images = $this->Model_user_trip_image->getMultipleRows(array('trip_id' => $insert_id),true);
                if($trip_images){
                    $trip_data['images'] = $trip_images;
                }
                $trip_data['members'] = $this->Model_user->getUsers($insert_id);

                $this->response([
                    'status' => TRUE,
                    'trip_detail' => $trip_data
                ], REST_Controller::HTTP_OK);
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }



    public function getTripImages_post(){
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            
            $fetch_by = array();
            $fetch_by['trip_id'] = $post_data['trip_id'];
            if(isset($post_data['user_id'])){
                $fetch_by['user_id'] = $post_data['user_id'];
            }

            $return_array = array();

            $images = $this->Model_user_trip_image->getMultipleRows($fetch_by);
            if($images){
                $return_array = $images;
            }
         
                
            

                $this->response([
                    'status' => TRUE,
                    'images' => $return_array
                ], REST_Controller::HTTP_OK);
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function getNotification_get(){
         $post_data = $this->input->get();
        if (!empty($post_data)) {
            $fetch_trip = array();
            $fetch_trip['user_id'] = $post_data['user_id'];
            $member_of  = $this->Model_trip_member->getMultipleRows($fetch_trip,true);
            $retutn_array = array();
            if($member_of){
                $trip_ids = array_column($member_of, 'trip_id');
                $notifications = $this->Model_notification->getNotification($trip_ids);
                if($notifications){
                    foreach ($notifications as $key => $value) {
                        //print_r($value);exit;
                                $user_data = $this->Model_user->get($value['user_id'],true,'user_id');
                                $trip_data = $this->Model_user_trip->get($value['trip_id'],true,'user_trip_id');
                               // $replace =array('[user]','[trip]'); 
                                $value['username'] = $username = '';
                                if(isset($user_data['username'])){
                                    $value['username'] = $username = $user_data['username'];

                                }

                                $value['title'] = $title = '';

                                if(isset($trip_data['title'])){
                                    $value['title'] = $title = $trip_data['title'];
                                }

                               // $replace_by =array($username,$title);
                                //$value['notification'] = str_replace($replace, $replace_by,$value['notification']);
                                $retutn_array[] = $value;


                    }

                }
            }
                
            

                $this->response([
                    'status' => TRUE,
                    'notifications' => $retutn_array
                ], REST_Controller::HTTP_OK);
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function removeImage_post(){
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            
            $deleted_by = array();
            $deleted_by['trip_image_id'] = $post_data['trip_image_id'];
            $this->Model_user_trip_image->delete($deleted_by);
            $this->Model_trip_comment->delete($deleted_by);
                
            

                $this->response([
                    'status' => TRUE,
                    'message' => 'Deleted successfully'
                ], REST_Controller::HTTP_OK);
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function removeComment_post(){
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            
            $deleted_by = array();
            $deleted_by['trip_comment_id'] = $post_data['trip_comment_id'];
            $this->Model_trip_comment->delete($deleted_by);
                
            

                $this->response([
                    'status' => TRUE,
                    'message' => 'Deleted successfully'
                ], REST_Controller::HTTP_OK);
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function leaveTrip_post(){
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            
            $deleted_by = array();
            $deleted_by['user_id'] = $post_data['user_id'];
            $deleted_by['trip_id'] = $post_data['trip_id'];
            $this->Model_trip_member->delete($deleted_by);
            

                $this->response([
                    'status' => TRUE,
                    'message' => 'Leave successfully'
                ], REST_Controller::HTTP_OK);
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function saveTripComment_post()
    {
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            
            
            $post_data['created_at'] = date('Y-m-d H:i:s');
            $insert_id = $this->Model_trip_comment->save($post_data);
            if($insert_id > 0)
            {

                $notification_save = array();
                $notification_save['user_id'] =  $post_data['user_id'];
                $notification_save['trip_id'] =  $post_data['trip_id'];
                $notification_save['image_id'] =  $post_data['trip_image_id'];
                $notification_save['notification'] =  '[user] left a comment on a photo in [trip]';
                $notification_save['created_at'] =  time();
                $this->Model_notification->save($notification_save);

                $this->response([
                    'status' => TRUE,
                    'message' => 'Comment save successfully'
                ], REST_Controller::HTTP_OK);
            }else{
                
                $this->response([
                 'status' => FALSE,
                 'message' => 'There is something went wrong'
                ], REST_Controller::HTTP_OK);
            }
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
 public function getTripImageData_get(){
        $post_data = $this->input->get();
        if (!empty($post_data)) {
                
                $trip_data = $this->Model_user_trip_image->get($post_data['trip_image_id'],true,'trip_image_id');
               
                $trip_data['comments'] = $this->Model_trip_comment->getComments('trip_comment.trip_image_id = '.$post_data['trip_image_id']);
                

                $this->response([
                    'status' => TRUE,
                    'image_detail' => $trip_data
                ], REST_Controller::HTTP_OK);
            
            if($insert_id > 0)
            {

                $this->response([
                    'status' => TRUE,
                    'message' => 'Comment save successfully'
                ], REST_Controller::HTTP_OK);
            }else{
                
                $this->response([
                 'status' => FALSE,
                 'message' => 'There is something went wrong'
                ], REST_Controller::HTTP_OK);
            }
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getTripData_get(){
        $post_data = $this->input->get();
        if (!empty($post_data)) {
                
                $trip_data = $this->Model_user_trip->get($post_data['user_trip_id'],true,'user_trip_id');

                $fetch_images_by = array();
                $fetch_images_by['trip_id'] = $post_data['user_trip_id'];
                if(isset($post_data['user_id'])){
                    $fetch_images_by['user_id'] = $post_data['user_id'];
                }
                $trip_images = $this->Model_user_trip_image->getMultipleRows($fetch_images_by,true);
                $trip_data['images'] = array();
                $trip_data['image_count'] = 0;
                $trip_data['member_count'] = 0;
                if($trip_images){
                    
                         $trip_data['image_count'] = count($trip_images);
                            
                    foreach ($trip_images as $key2 => $value2){
                        $trip_data['images'][$key2] = $value2;
                        $trip_data['images'][$key2]['comments'] = $this->Model_trip_comment->getComments('trip_comment.trip_image_id = '.$value2['trip_image_id']);
                    }
                        

                       // $trip_array[$key]['members'] = $this->Model_user->getUsers($value['user_trip_id']);
                }
                $trip_data['members'] = $members = $this->Model_user->getUsers($post_data['user_trip_id']);
                $trip_data['member_count'] = count($members);
                //$trip_data['comments'] = $this->Model_trip_comment->getComments('trip_comment.trip_id = '.$post_data['user_trip_id']);

                $this->response([
                    'status' => TRUE,
                    'trip_detail' => $trip_data
                ], REST_Controller::HTTP_OK);
            
            if($insert_id > 0)
            {

                $this->response([
                    'status' => TRUE,
                    'message' => 'Comment save successfully'
                ], REST_Controller::HTTP_OK);
            }else{
                
                $this->response([
                 'status' => FALSE,
                 'message' => 'There is something went wrong'
                ], REST_Controller::HTTP_OK);
            }
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function getAllTrips_get(){
         $post_data = $this->input->get();
        
                $where = false;
                if(isset($post_data['user_id'])){
                    $where  = 'trip_members.user_id ='.$post_data['user_id'];
                }
                
                $trips = $this->Model_user_trip->getAllTrips($where);
                $trip_array = array();
                if($trips){
                    foreach ($trips as $key => $value) {
                       $trip_array[$key] = $value;
                       $trip_array[$key]['image_count'] = 0;
                       $trip_array[$key]['member_count'] = 0;
                       $trip_images = $this->Model_user_trip_image->getMultipleRows(array('trip_id' => $value['user_trip_id']),true);
                        $trip_array[$key]['images'] = array();
                        if($trip_images){
                            $trip_array[$key]['image_count'] = count($trip_images);
                            $trip_array[$key]['images'] = $trip_images;
                           /* foreach ($trip_images as $key2 => $value2){
                                $trip_array[$key]['images'][$key2] = $value2;
                                $trip_array[$key]['images'][$key2]['comments'] = $this->Model_trip_comment->getComments('trip_comment.trip_image_id = '.$value2['trip_image_id']);
                            }*/
                        }
                        
                         $members = $this->Model_user->getUsers($value['user_trip_id']);

                         if(isset($post_data['user_id'])){
                           if(!empty($members)){
                            $user_arr = array();
                               
                                $user_arr[] = $this->Model_user->get($post_data['user_id'],true,'user_id');
                                foreach ($members as $value) {

                                   if($post_data['user_id'] != $value['user_id']){

                                            $user_arr[] = $value;

                                   }
                                }

                              

                                $users = $user_arr;
                                

                            }

                            $trip_array[$key]['members'] = $users;
                            $trip_array[$key]['member_count'] = count($users);


                        }else{
                            $trip_array[$key]['members'] = $members;
                            $trip_array[$key]['member_count'] = count($members);
                        }

                        
                       
                       
                    }
                    
                    

                    $this->response([
                        'status' => TRUE,
                        'trip_detail' => $trip_array
                    ], REST_Controller::HTTP_OK);

                }else{
                
                $this->response([
                 'status' => FALSE,
                 'message' => 'No trip found'
                ], REST_Controller::HTTP_OK);
            }

                
            
           
        
    }
    
    
    
    public function saveChatGroup_post()
    {
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            
            
            $post_data['created_at'] = date('Y-m-d H:i:s');
            
            
            $fetch = array();
            $fetch['group_id'] = $post_data['group_id'];
            $result = $this->Model_chat_group->getWithMultipleFields($fetch);
            if($result){
                $insert_id = $this->Model_chat_group->update($post_data,$fetch);
            }else{
                $insert_id = $this->Model_chat_group->save($post_data);
            }
            
            
            
            
            if($insert_id > 0)
            {
	        $chat_group_data = $this->Model_chat_group->get($post_data['group_id'], true,'group_id');  
                $this->response([
	                'status' => TRUE,
	                'chat_group_detail' => $chat_group_data
	            ], REST_Controller::HTTP_OK);
            }else{
            	
                $this->response([
                 'status' => FALSE,
                 'message' => 'There is something went wrong'
                ], REST_Controller::HTTP_OK);
            }
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
    
    public function saveMessageCount_post()
    {
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            
            
            
            
            
            $fetch = array();
            $fetch['chat_group_id'] = $post_data['chat_group_id'];
            $fetch['message_id'] = $post_data['message_id'];
            $fetch['user_id'] = $post_data['user_id'];
            $result = $this->Model_chat_message_count->getWithMultipleFields($fetch);
            if($result){
                $update = array();
                $update['count'] = $result->count + 1;
                $insert_id = $this->Model_chat_message_count->update($update,$fetch);
            }else{
                $post_data['count'] = 1;
                $insert_id = $this->Model_chat_message_count->save($post_data);
            }
            
            
            
            
            if($insert_id > 0)
            {
	        $chat_group_data = $this->Model_chat_message_count->get($post_data['message_id'], true,'message_id');  
                $this->response([
	                'status' => TRUE,
	                'chat_message_count_detail' => $chat_group_data
	            ], REST_Controller::HTTP_OK);
            }else{
            	
                $this->response([
                 'status' => FALSE,
                 'message' => 'There is something went wrong'
                ], REST_Controller::HTTP_OK);
            }
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
    
    public function getMessageCount_get()
    {
        $fetch = array();
        $message_count = array();
        $fetch['chat_group_id'] = $this->input->get('chat_group_id');
        $fetch['user_id'] = $this->input->get('user_id');
        $result = $this->Model_chat_message_count->getMultipleRows($fetch,true);
        if($result){
            $message_count = $result;
        }
        $this->response([
            'status' => TRUE,
            'message_count' => $message_count
        ], REST_Controller::HTTP_OK);

    }
    
    public function getChatGroup_get()
    {
        $post_data = $this->input->get(); // isset check is implemented in the model function for user_id
	if (!empty($post_data)) {
            $return_array = array();
            
            $chat_groups = $this->Model_chat_group->getAllChatGroups($post_data);
            $user_info   = $this->Model_user->get($post_data['user_id'],true);
            if($user_info){
                $my_first_name = $user_info['first_name'];
                $my_last_name  = $user_info['last_name'];
            
            foreach($chat_groups as $key => $chat_group){
                $check_block = array();
                if($chat_group['sender_id'] == $post_data['user_id']){
                    $check_block['blocked_user_id'] = $chat_group['receiver_id'];
                }else{
                    $check_block['blocked_user_id'] = $chat_group['sender_id'];
                }
                
                $check_block['blocked_by_user_id'] = $post_data['user_id'];
                $block_user_data = $this->Model_block_user->getWithMultipleFields($check_block);
                if(!$block_user_data){      
                    $chat_group['my_first_name'] = $my_first_name;
                    $chat_group['my_last_name']  = $my_last_name;
                    if($chat_group['sender_id'] == $post_data['user_id']){

                        $partner_info   = $this->Model_user->get($chat_group['receiver_id'],true);
                        if($partner_info){
                            $chat_group['partner_first_name'] = $partner_info['first_name'];
                            $chat_group['partner_last_name'] = $partner_info['last_name'];
                        }else{
                            $chat_group['partner_first_name'] = '';
                            $chat_group['partner_last_name'] = '';
                        }
                        $fetch_by = array();
                        $fetch_by['user_id'] = $chat_group['receiver_id'];
                        $status_image = $this->Model_status->getMultipleRows($fetch_by,true,'desc',1,0,'user_status_id');

                        if($status_image){
                            $chat_group['status_image'] = $status_image[0]['status_image'];
                        }else{
                            $chat_group['status_image'] = '';
                        }



                    }elseif($chat_group['receiver_id'] == $post_data['user_id']){

                        $partner_info   = $this->Model_user->get($chat_group['sender_id'],true);
                        if($partner_info){
                            $chat_group['partner_first_name'] = $partner_info['first_name'];
                            $chat_group['partner_last_name'] = $partner_info['last_name'];
                        }else{
                            $chat_group['partner_first_name'] = '';
                            $chat_group['partner_last_name'] = '';
                        }


                        $fetch_by = array();
                        $fetch_by['user_id'] = $chat_group['sender_id'];
                        $status_image = $this->Model_status->getMultipleRows($fetch_by,true,'desc',1,0,'user_status_id');

                        if($status_image){
                            $chat_group['status_image'] = $status_image[0]['status_image'];
                        }else{
                            $chat_group['status_image'] = '';
                        }

                    }

                    $return_array[] = $chat_group;
            } 
                
            }
            
            
            $this->response([
                 'status' => TRUE,
                 'chat_groups' => $return_array
                ], REST_Controller::HTTP_OK);
            }else{
                $this->response([
                 'status' => FALSE,
                 'message' => "User not exist"
                ], REST_Controller::HTTP_OK);
            }
            
            
            
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
    
    public function deleteStatus_post()
    {
        $post_data = $this->input->post();
        if (!empty($post_data)) {
             $this->Model_status_view->delete($post_data);
             $this->Model_status->delete($post_data);
             $this->response([
                 'status' => TRUE,
                 'message' => 'Deleted successfully'
                ], REST_Controller::HTTP_OK);
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
    public function getUserStatus_get()
    {
        $post_data = $this->input->get();
        if (!empty($post_data)) {
            
            $user_data = $this->Model_user->getUserData("users.user_id = ".$post_data['user_id']);
            
            if($user_data){
                $status = array();
               
                $get_status = $this->Model_status->getUserStatus($post_data['user_id']);
                if(!empty($get_status)){
                    $user_status = array();
                    $count_status = 0;
                    foreach ($get_status as $status){
                        $fetch_by = array();
                        $fetch_by['user_status_id'] = $status['user_status_id'];
                        $res = $this->Model_status_view->getMultipleRows($fetch_by);
                        if($res){
                            $count_status = count($res);
                        }
                        $status['total_viewed'] = $count_status;
                        $user_status[] = $status;
                        
                        
                    }
                    
                    $status = $user_status;
                }
                
                
                $this->response([
                 'status' => TRUE,
                 'user_info' => $user_data,
                 'user_status' => $status
                ], REST_Controller::HTTP_OK); 
                
            }else{
               $this->response([
                 'status' => FALSE,
                 'message' => 'User not found'
                ], REST_Controller::HTTP_OK); 
            }
            
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
     public function getSingleStatus_get(){
         $post_data = $this->input->get();
        if (!empty($post_data)) {
            
            $status_data = $this->Model_status->getStatusData('user_status.user_status_id = '.$post_data['user_status_id']);
           
            
            if($status_data){
                $status = array();
                $fetch_by = array();
                $fetch_by['user_status_id'] = $status_data['user_status_id'];
                $count_status = 0;
                $res = $this->Model_status_view->getMultipleRows($fetch_by);
                if($res){
                    $count_status = count($res);
                }
                $status_data['total_viewed'] = $count_status;
                
                $this->response([
                 'status' => FALSE,
                 'status_info' => $status_data
                ], REST_Controller::HTTP_OK); 
                
            }else{
               $this->response([
                 'status' => FALSE,
                 'message' => 'Status not found'
                ], REST_Controller::HTTP_OK); 
            }
            
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
     }


    public function categories_get()
    {
        $post_data = $this->input->get();
        $categories = $this->Model_category->getAll(true);
        $return_main_array = array();
        if (!empty($post_data)) {
            if($categories){
                foreach ($categories as $key => $value) {
                $post_data['distance'] = 30; 
                $post_data['category_id'] = $value['category_id'];
                $users = $this->Model_user->getNearestUsers($post_data);

                $return_array = array();
                if(!empty($users)){
                       
                    foreach($users as $user){
                            
                            $fetch_by = array();
                            $fetch_by['user_id'] = $user['user_id'];
                            $user['over_all_viewed'] = 1;
                            $status = $this->Model_status->getUserStatus($user['user_id']);
                            if(!empty($status)){
                                    $user_status = array();
                                    $count_status = 0;
                                    foreach ($status as $status){
                                        $fetch_by = array();
                                        $fetch_by['user_status_id'] = $status['user_status_id'];
                                        $res = $this->Model_status_view->getMultipleRows($fetch_by);
                                        if($res){
                                            $count_status = count($res);
                                        }
                                        $status['total_viewed'] = $count_status;

                                        $fetch_by['user_id'] = isset($post_data['user_id']) ? $post_data['user_id'] : 0;
                                        $res2 = $this->Model_status_view->getMultipleRows($fetch_by);
                                        if($res2){
                                           $status['is_viewed']  = 1;//viewed by user
                                        }else{
                                            $status['is_viewed']  = 0;//not viewed by user 
                                        }
                                        if($status['is_viewed'] == 0){
                                             $user['over_all_viewed'] = 0;
                                        }

                                        $user_status[] = $status;

                                    }
                                    /*usort($user_status, function($a, $b) {
                                            return  $a['is_viewed'] - $b['is_viewed'];
                                        });*/
                                    $user['user_status'] = $user_status;
                                    //$return_array[] = $user;  
                                }else{
                                    $user['user_status'] = array();
                                }

                                $return_array[] = $user;
                      
                    }
                    usort($return_array, function($a, $b) {
                        return  $a['over_all_viewed'] - $b['over_all_viewed'];
                                    });
                    $value['users'] = $return_array;
                    
                   /* usort($return_array, function($a, $b) {
                        return  $a['over_all_viewed'] - $b['over_all_viewed'];
                                    });*/
                                
                   

                }else{
                   $value['users'] = array(); 
                } 
                    
                $return_main_array[] = $value;
            
           }

            $this->response([
                     'status' => TRUE,
                     'categories' => $return_main_array
                    ], REST_Controller::HTTP_OK); 
       }else{
            $this->response([
                     'status' => FALSE,
                     'categories' => 'No data found'
                    ], REST_Controller::HTTP_OK);
       } 
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    } 
    
    public function getNearestUsers_get()
    {
        $post_data = $this->input->get();
        if (!empty($post_data)) {
            $get_user = array();
            $get_user = $this->Model_user->get($post_data['user_id'],true);
            if($get_user){
                
                if((!isset($post_data['location_latitude']) && !isset($post_data['location_longitude'])) || ($get_user['location_longitude'] != '' && $get_user['location_longitude'] != '')){
                   // print_r($get_user);exit;
                  if(isset($post_data['location_latitude']) && isset($post_data['location_longitude'])){
                     $get_user['location_latitude'] =  $post_data['location_latitude'];
                     $get_user['location_longitude'] =  $post_data['location_longitude'];
                  }  
                 // print_r($get_user);exit;
                $users = $this->Model_user->getNearestUsers($get_user);
                
               
                $return_array = array();
                $my_status = array();
                
                $get_user['user_status'] = $this->Model_status->getUserStatus($post_data['user_id']);
                if($get_user['user_status']){
                    $get_user['over_all_viewed'] = 1;
                    $get_user['group_id'] = '';
                    $user_status = array();
                    $count_status = 0;
                    foreach ($get_user['user_status'] as $status){
                        $fetch_by = array();
                        $fetch_by['user_status_id'] = $status['user_status_id'];
                        $res = $this->Model_status_view->getMultipleRows($fetch_by);
                        if($res){
                            $count_status = count($res);
                        }
                        
                        $status['total_viewed'] = $count_status;
                        $fetch_by['user_id'] = $post_data['user_id'];
                        $res2 = $this->Model_status_view->getMultipleRows($fetch_by);
                        if($res2){
                           $status['is_viewed']  = 1;//viewed by user
                        }else{
                            $status['is_viewed']  = 0;//not viewed by user 
                        }
                        
                         if($status['is_viewed'] == 0){
                            $get_user['over_all_viewed'] = 0;
                          }
                       
                        
                        $user_status[] = $status;
                        
                        
                    }
                    /*usort($user_status, function($a, $b) {
				    return  $a['is_viewed'] - $b['is_viewed'];
                                });*/
                    $get_user['user_status'] = $user_status;
                    $my_status[] = $get_user;
                }
                
                
                if(!empty($users)){
                    
                    foreach($users as $user){
                        
                        $check_block = array();
                        $check_block['blocked_user_id'] = $user['id'];
                        $check_block['blocked_by_user_id'] = $post_data['user_id'];
                        $block_user_data = $this->Model_block_user->getWithMultipleFields($check_block);
                       
                        if(!$block_user_data){
                            $fetch_by = array();
                            $fetch_by['user_id']    =  $post_data['user_id'];
                            $fetch_by['chat_with']  =   $user['id'];
                            $getDialogID            =  $this->Model_chat_group->getChatID($post_data['user_id'],$user['id']);
                            if($getDialogID)
                            {
                                $user['group_id'] = $getDialogID->group_id;
                            }else
                            {
                                $user['group_id'] = '';
                            }

                            $user['over_all_viewed'] = 1;



                            $fetch_by = array();
                            $fetch_by['user_id'] = $user['id'];
                            $status = $this->Model_status->getUserStatus($user['id']);
                            if(!empty($status)){
                                $user_status = array();
                                $count_status = 0;
                                foreach ($status as $status){
                                    $fetch_by = array();
                                    $fetch_by['user_status_id'] = $status['user_status_id'];
                                    $res = $this->Model_status_view->getMultipleRows($fetch_by);
                                    if($res){
                                        $count_status = count($res);
                                    }
                                    $status['total_viewed'] = $count_status;

                                    $fetch_by['user_id'] = $post_data['user_id'];
                                    $res2 = $this->Model_status_view->getMultipleRows($fetch_by);
                                    if($res2){
                                       $status['is_viewed']  = 1;//viewed by user
                                    }else{
                                        $status['is_viewed']  = 0;//not viewed by user 
                                    }
                                    if($status['is_viewed'] == 0){
                                         $user['over_all_viewed'] = 0;
                                    }

                                    $user_status[] = $status;

                                }
                                /*usort($user_status, function($a, $b) {
                                        return  $a['is_viewed'] - $b['is_viewed'];
                                    });*/
                                $user['user_status'] = $user_status;
                                $return_array[] = $user;  
                            }
                    }
                   }

                    usort($return_array, function($a, $b) {
				    return  $a['over_all_viewed'] - $b['over_all_viewed'];
                                });
                                
                    $this->response([
                     'status' => TRUE,
                     'my_status' => $my_status,  
                     'users' => $return_array
                    ], REST_Controller::HTTP_OK); 

                }else{
                   $this->response([
                     'status' => TRUE,
                      'my_status' => $my_status, 
                     'users' => $return_array
                    ], REST_Controller::HTTP_OK); 
                } 
                }else{
                     $this->response([
                     'status' => FALSE,
                     'message' => 'Please add your location first'
                    ], REST_Controller::HTTP_OK);
                }
                
            }else{
                $this->response([
                     'status' => FALSE,
                     'message' => 'User not found'
                    ], REST_Controller::HTTP_OK);
            }
            
            
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
    public function saveStatusView_post()
    {
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            
            $already_checked = $this->Model_status_view->getWithMultipleFields($post_data);
            if($already_checked){
                $this->response([
                     'status' => FALSE,
                     'message' => 'User already viewd this status'
                    ], REST_Controller::HTTP_OK);
            }else{
                $file_name = '';

                $post_data['created_at'] = date('Y-m-d H:i:s');
                $insert_id = $this->Model_status_view->save($post_data);
                if($insert_id > 0)
                {
                    $status_view_data = $this->Model_status_view->get($insert_id, true,'status_view_id');  
                    $this->response([
                            'status' => TRUE,
                            'status_view_detail' => $status_view_data
                        ], REST_Controller::HTTP_OK);
                }else{

                    $this->response([
                     'status' => FALSE,
                     'message' => 'There is something went wrong'
                    ], REST_Controller::HTTP_OK);
                }
            }
                
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
    public function saveDialog_post()
    {
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            
            $fetch_by = array();
            $fetch_by['user_id'] = $post_data['user_id'];
            $fetch_by['chat_with']  = $post_data['chat_with'];
            
            $dialog = $this->Model_dialog->getWithMultipleFields($fetch_by,true);
            if($dialog)
            {
	            $this->response([
	                'status' => TRUE,
	                'dialog_detail' => $dialog
	            ], REST_Controller::HTTP_OK);
            }else{
            	$insert_id = $this->Model_dialog->save($post_data);
                $dialog = $this->Model_dialog->get($insert_id, true,'user_dialog_id');
                $this->response([
                 'status' => TRUE,
                 'dialog_detail' => $dialog
                ], REST_Controller::HTTP_OK);
            }
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
    
    public function saveReport_post()
    {
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            
            
            $insert = $this->Model_report->save($post_data);
            if($insert > 0){
                $this->response([
                'status' => TRUE,
                'message' => 'Reported Successfully'
            ], REST_Controller::HTTP_OK);
            }
            
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
    
    public function blockUser_post()
    {
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            
            
            $insert = $this->Model_block_user->save($post_data);
            if($insert > 0){
                $this->response([
                'status' => TRUE,
                'message' => 'Blocked Successfully'
            ], REST_Controller::HTTP_OK);
            }
            
            
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
    
  

    public function getUser_get()
    {
        $user_id = $this->input->get('user_id');
        $user = $this->Model_user->getUserData('users.user_id ='.$user_id);
        
      
        if ($user) {
            $this->response([
                'status' => TRUE,
                'user_info' => $user
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST);
        }
    }

   

}

