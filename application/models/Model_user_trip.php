<?php
Class Model_user_trip extends Base_Model
{
	public function __construct()
	{
		parent::__construct("user_trips");
		
	}

	public function getAllTrips($where = false)
    {
        
        $this->db->select("user_trips.*,users.name,users.username");
        $this->db->from('users');
        $this->db->join('user_trips','user_trips.user_id = users.user_id');
        $this->db->join('trip_members','trip_members.trip_id = user_trips.user_trip_id');
        if($where){
        	 $this->db->where($where);
        }

        $this->db->group_by('user_trips.user_trip_id');
       
        return $this->db->get()->result_array();
        

    } 
	
   
}