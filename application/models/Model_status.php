<?php
Class Model_status extends Base_Model
{
    public function __construct()
    {
        parent::__construct("user_status");

    }



     public function getNearestStatus($posted_data)
    {
        
        $this->db->select("user_status.*, ( 6371 * acos( cos( radians(".$posted_data['location_latitude'].") ) * cos( radians( user_status.status_latitude ) ) * cos( radians( user_status.status_longitude ) - radians(".$posted_data['location_longitude'].") ) + sin( radians(".$posted_data['location_latitude'].") ) * sin( radians( user_status.status_latitude ) ) ) ) AS distance,users.address,users.phone,users.business_name");

        $this->db->from('user_status');   

        $this->db->join('users','users.user_id = user_status.user_id');
        $this->db->join('categories','users.category_id = categories.category_id');    
        
        $this->db->where('categories.category_id',$posted_data['category_id']);
        
        $this->db->having('distance <=',$posted_data['distance']);
        return $this->db->get()->result_array();
        

    }


    public function deletePreviousStatus($d_date){
            $this->db->query("Delete from user_status where created_at < '".$d_date."'");
            return true;
            
    }
    
    
    public function getStatus($d_date){
        return $this->db->query("Select * from user_status where created_at < '".$d_date."'")->result_array();
    }
    
    public function getUserStatus($user_id){
        $this->db->select("user_status.*,categories.title as category_title,users.business_name,users.phone");
        $this->db->from('user_status');
        $this->db->join('users','users.user_id = user_status.user_id');
        $this->db->join('categories','categories.category_id = users.category_id','left');
        $this->db->where('user_status.user_id',$user_id);
        $this->db->order_by('user_status.user_status_id','desc');
        return $this->db->get()->result_array();
        
        
    }


    public function getStatusData($where = false)
    {
        
        $this->db->select("user_status.*,categories.title as category_title,users.business_name,users.phone");
        $this->db->from('user_status');
        $this->db->join('users','users.user_id = user_status.user_id');
        $this->db->join('categories','categories.category_id = users.category_id','left');
        $this->db->where($where);
        return $this->db->get()->row_array();
        

    }   


}