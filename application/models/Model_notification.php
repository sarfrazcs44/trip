<?php
Class Model_notification extends Base_Model
{
	public function __construct()
	{
		parent::__construct("notifications");
		
	}


    public function getNotification($trip_id = array())
    {

        $this->db->select('*');
        $this->db->from('notifications');
        $this->db->where_in('trip_id',$trip_id);
        return $this->db->get()->result_array();


    }
	
  
}