<?php
Class Model_chat_group extends Base_Model
{
    public function __construct()
    {
        parent::__construct("chat_groups");

    }
    
    
    
    public function getAllChatGroups($post_data){
        $this->db->select('user_status.status_image,chat_groups.*');
        $this->db->from('chat_groups');
        $this->db->join('user_status','user_status.user_status_id = chat_groups.status_id','left');
        
        $where = 'chat_groups.sender_id = '.$post_data['user_id'].' OR chat_groups.receiver_id = '.$post_data['user_id'].'';
        $this->db->where($where);
        return $this->db->get()->result_array();
        
    }
    
    
     public function getChatID($user_id,$chat_with){
        $this->db->select('chat_groups.*');
        $this->db->from('chat_groups');
       
        
        $where = '(chat_groups.sender_id = '.$user_id.' AND chat_groups.receiver_id = '.$chat_with.') OR (chat_groups.sender_id = '.$chat_with.' AND chat_groups.receiver_id = '.$user_id.')';
        $this->db->where($where);
        return $this->db->get()->row();
        
    }
    
}

    