<?php
Class Model_user extends Base_Model
{
	public function __construct()
	{
		parent::__construct("users");
		
	}
	
    public function getNearestUsers($posted_data)
    {
        
        $this->db->select("users.*, ( 6371 * acos( cos( radians(".$posted_data['location_latitude'].") ) * cos( radians( users.user_latitude ) ) * cos( radians( users.user_longitude ) - radians(".$posted_data['location_longitude'].") ) + sin( radians(".$posted_data['location_latitude'].") ) * sin( radians( users.user_latitude ) ) ) ) AS distance");
            
        $this->db->from('users');
        //$this->db->where('id !=',$posted_data['id']);
        $this->db->where('category_id',$posted_data['category_id']);
        $this->db->having('distance <=',$posted_data['distance']);
        return $this->db->get()->result_array();
        

    }


    public function getUserData($where)
    {
        
        $this->db->select("users.*");
        $this->db->from('users');
        //$this->db->join('categories','categories.category_id = users.category_id','left');
        $this->db->where($where);
        return $this->db->get()->row_array();
        

    }   

    public function searchUsers($user_name)
    {
        
        $this->db->select("*");
        $this->db->from('users');
        //$this->db->join('categories','categories.category_id = users.category_id','left');
        $this->db->like('username',$user_name,'both');
        return $this->db->get()->result_array();
        

    }


     public function getUsers($trip_id)
    {
        
        $this->db->select("users.*");
        $this->db->from('users');

        $this->db->join('trip_members','trip_members.user_id = users.user_id');
        
        $this->db->where('trip_members.trip_id',$trip_id);
        return $this->db->get()->result_array();
        

    }

    




}