<?php
Class Model_trip_comment extends Base_Model
{
	public function __construct()
	{
		parent::__construct("trip_comment");
		
	}

	public function getComments($where = false)
    {
        
        $this->db->select("users.name,users.username,trip_comment.*");
        $this->db->from('users');
        $this->db->join('trip_comment','trip_comment.user_id = users.user_id','left');
        if($where){
        	 $this->db->where($where);
        }
       
        return $this->db->get()->result_array();
        

    } 
	
   
}