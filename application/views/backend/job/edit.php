<?php
$category_option = '<option value="">Select Category</option>';
  if($categories){
      
      foreach($categories as $category){
          $category_option .= '<option value="'.$category->category_id.'" '.($category->category_id == $result->category_id ? 'selected' : '').'>'.$category->title.'</option>';
      }
  }
    
?>
<style>
 .pac-container {
    z-index: 10000;
}

.btn.confirm-loc-btn.btn-success {
    color: #fff;
    display: block;
    padding: 10px !important;
    width: 150px;
}
.address_map {
    width: 100%;
    height: 400px;
}
  
    
    
</style>
<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Edit Job</h4>
                               
                                <form action="<?php echo base_url();?>cms/job/action" method="post" onsubmit="return false;" class="GeoDetails form_data" enctype="multipart/form-data" data-parsley-validate=""> 
                                    <input type="hidden" name="form_type" value="update">
                                    <input type="hidden" name="job_id" value="<?php echo $job_id;?>">
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div> 
                                    
                                    <div class="form-group">
                                       <label for="title-eng">Title:</label>
                                       <input type="text" class="form-control" name="title" id="title-eng" required value="<?php echo $result->title; ?>">
                                     </div>
                                     
                                    <div class="form-group">
                                        <label for="category">Choose Parent Category :</label>
                                        <select id="category" name="category_id" class="form-control" required="" >
                                            <?php echo $category_option; ?>
                                        </select>
                                    </div>
                                    
                                    
                                     <div class="form-group">
                                       <label for="address">Address :</label>
                                       <input type="text" class="form-control" name="address" id="address" required data-geo="formatted_address" data-toggle="modal" data-target="#locationModal" value="<?php echo $result->address; ?>">
                                    </div>
                                    <div class="form-group">
                                       <label for="city">City :</label>
                                       <input type="text" class="form-control" name="city" id="city" required value="<?php echo $result->city; ?>">
                                    </div>
                                    
                                    <div class="form-group">
                                       <label for="title-eng">Country :</label>
                                       <input type="text" class="form-control" name="country" id="country" required value="<?php echo $result->country; ?>">
                                    </div>
                                    
                                    
                                     <div class="form-group">
                                       <label for="price">Price :</label>
                                       <input type="text" class="form-control" name="price" id="price" required value="<?php echo $result->price; ?>">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="shift_type">Shift Type :</label>
                                        <select id="shift_type" name="shift_type" class="form-control" required="" >
                                            <option value="Full Time" <?php echo ($result->shift_type == 'Full Time' ? 'selected' : ''); ?> >Full Time</option>
                                            <option value="Part Time" <?php echo ($result->shift_type == 'Part Time' ? 'selected' : ''); ?>>Part Time</option>
                                        </select>
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                        <label for="job_type">Job Type :</label>
                                        <select id="job_type" name="job_type" class="form-control" required="" >
                                            <option value="Individuals" <?php echo ($result->job_type == 'Individuals' ? 'selected' : ''); ?>>Individuals</option>
                                            <option value="Business" <?php echo ($result->job_type == 'Business' ? 'selected' : ''); ?>>Business</option>
                                        </select>
                                    </div>
                                    
                                    
                                    
                                    <div class="form-group">
	                                   <label>Description:</label>
                                        <textarea class="form-control" rows="5" name="description"><?php echo $result->description; ?></textarea>
	                                               
	                            </div>
                                    
                                    <?php if($result->image != ''){ ?>
                                    <img src="<?php echo base_url($result->image);?>" alt="image" class="img-responsive img-thumbnail" width="200" style="height:200px;"/>
                                    <?php } ?>
                                    <div class="form-group">
                                    <label for="image">Please Choose Image :</label>
                                    <input type="file" class="filestyle" id="image" name="image[]" data-placeholder="No Image">
                                    </div>
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        
                                        
                                        
                                    </div>
                                     </div>
                                     <input name="latitude" type="hidden" data-geo="lat" value="<?php echo $result->latitude; ?>">
			            <input name="longitude" type="hidden" data-geo="lng" value="<?php echo $result->longitude; ?>">
                                    </form>
                                </div>
                            </div> <!-- end col -->

                            
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->
    
<div class="modal fade" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="locationModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
			<div class="form-group">
                                 
                            <input type="text" class="form-control geocomplete_map_draggable" style="width:70%;float:left;">
                            <a class="btn confirm-loc-btn btn-success" data-dismiss="modal" style="float:right;">Confirm</a>
                        </div>	
				
			</div>
			<div class="modal-body">
				<div class="address_map"></div>
			</div>
		</div>
	</div>
</div><!--/modal-->