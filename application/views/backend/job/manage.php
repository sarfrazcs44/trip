<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Jobs</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        
                                       <li>
                                        <a href="<?php echo base_url('cms/job/add');?>">
                                           <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Job</button>
                                        </a>
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;"></div>
                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Title</th>
                                            <th>User Name</th>
                                            <th>City</th>
                                          
                                            <th>Price</th>
                                            <th>Price Type</th>
                                            <th>Shift Type</th>
                                            <th>Job Type</th>
                                            <th>Created At</th>
                                            <th>Action</th>
                                            
                                            
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($jobs){
                                                foreach($jobs as $job){ ?>
                                                    <tr id="<?php echo $job['job_id'];?>">
                                                    <th>
                                                        <?php 
                                                        if(file_exists($job['image'])) { ?>
                                                                <img src="<?php echo base_url($job['image']);?>" alt="image"
                                                         class="img-responsive thumb-sm"/>
                                                        <?php }
                                                        ?>
                                                        
                                                    </th>
                                                    <th><?php echo $job['title']; ?></th>
                                                    <th><?php echo $job['first_name']; ?></th>
                                                    <th><?php echo $job['city']; ?></th>
                                                    <th><?php echo $job['price']; ?></th>
                                                    <th><?php echo $job['price_type']; ?></th>
                                                    <th><?php echo $job['shift_type']; ?></th>
                                                    <th><?php echo $job['job_type']; ?></th>
                                                    <th><?php echo $job['created_at']; ?></th>
                                                    <th>
                                                       
                                                        <a href="<?php echo base_url('cms/job/edit/'.$job['job_id']);?>">
                                                            <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-pencil"></i></button></a>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $job['job_id'];?>','cms/job/action','')">
                                                        <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i> </button></a>
                                                        </th> 
                                                    
                                                   
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->