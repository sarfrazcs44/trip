<?php
$category_option = '<option value="">Select Category</option>';
  if($categories){
      
      foreach($categories as $category){
          $category_option .= '<option value="'.$category->category_id.'" >'.$category->title.'</option>';
      }
  }
    
?>
<style>
 
/************* custom styles ****************/
.pac-container {
    z-index: 10000;
}

.btn.confirm-loc-btn.btn-success {
    color: #fff;
    display: block;
    padding: 10px !important;
    width: 150px;
}
.address_map {
    width: 100%;
    height: 400px;
}
   
</style>
<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Add Job</h4>
                               
                                <form action="<?php echo base_url();?>cms/job/action" method="post" onsubmit="return false;" class="form_data GeoDetails" enctype="multipart/form-data" data-parsley-validate=""> 
                                    <input type="hidden" name="form_type" value="save">
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div> 
                                    
                                    <div class="form-group">
                                       <label for="title-eng">Title :</label>
                                       <input type="text" class="form-control" name="title" id="title-eng" required value="">
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                        <label for="category">Choose Parent Category :</label>
                                        <select id="category" name="category_id" class="form-control" required="">
                                            <?php echo $category_option; ?>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group">
                                       <label for="address">Address :</label>
                                       <input type="text" class="form-control" name="address" id="address" required data-geo="formatted_address" data-toggle="modal" data-target="#locationModal" value="">
                                    </div>
                                    <div class="form-group">
                                       <label for="city">City :</label>
                                       <input type="text" class="form-control" name="city" id="city" required  value="">
                                    </div>
                                    
                                    <div class="form-group">
                                       <label for="title-eng">Country :</label>
                                       <input type="text" class="form-control" name="country" id="country" required value="">
                                    </div>
                                    
                                    <div class="form-group">
                                       <label for="price">Price :</label>
                                       <input type="text" class="form-control" name="price" id="price" required value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="shift_type">Shift Type :</label>
                                        <select id="shift_type" name="shift_type" class="form-control" required="" >
                                            <option value="Full Time">Full Time</option>
                                            <option value="Part Time">Part Time</option>
                                        </select>
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                        <label for="job_type">Job Type :</label>
                                        <select id="job_type" name="job_type" class="form-control" required="" >
                                            <option value="Individuals">Individuals</option>
                                            <option value="Business">Business</option>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group">
	                                   <label>Description:</label>
                                        <textarea class="form-control" rows="5" name="description"></textarea>
	                                               
	                            </div>
                                    
                                    <div class="form-group">
                                    <label for="image">Please Choose Image :</label>
                                    <input type="file" class="filestyle" id="image" name="image[]" data-placeholder="No Image">
                                    </div>
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        
                                        
                                    </div>
                                     </div>
                                    <input name="latitude" type="hidden" data-geo="lat" value="">
			            <input name="longitude" type="hidden" data-geo="lng" value="">
                                 </form>
                                </div>
                            </div> <!-- end col -->

                            
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->
    

<div class="modal fade" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="locationModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
			<div class="form-group">
                                 
                            <input type="text" class="form-control geocomplete_map_draggable" style="width:70%;float:left;">
                            <a class="btn confirm-loc-btn btn-success" data-dismiss="modal" style="float:right;">Confirm</a>
                        </div>	
				
			</div>
			<div class="modal-body">
				<div class="address_map"></div>
			</div>
		</div>
	</div>
</div><!--/modal-->
