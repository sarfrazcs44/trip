<!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <ul>
                        	<li class="menu-title">Navigation</li>

                            <li>
                                <a href="<?php echo base_url('cms/category');?>" class="waves-effect"><i class="mdi mdi-format-list-bulleted"></i><span> Categories </span></a>
                            </li>

                            <li>
                                <a href="<?php echo base_url('cms/user');?>" class="waves-effect"><i class="mdi mdi-diamond"></i><span> Users </span></a>
                            </li>
                            
                            <li>
                                <a href="<?php echo base_url('cms/job');?>" class="waves-effect"><i class="mdi mdi-heart-outline"></i><span> Jobs </span></a>
                            </li>
                            
                          
                            

                            


                            <li class="menu-title">More</li>

                            <li class="has_sub">
                                <a href="<?php echo base_url('cms/account/logout');?>" class="waves-effect"><i class="ti-power-off m-r-5"></i><span> Logout </span> <span class="menu-arrow"></span></a>
                               
                            </li>

                            
                        </ul>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                    <div class="help-box">
                        <h5 class="text-muted m-t-0">For Help ?</h5>
                        <p class=""><span class="text-custom">Email:</span> <br/> yjobs@support.com</p>
                        <p class="m-b-0"><span class="text-custom">Call:</span> <br/> (+123) 123 456 789</p>
                    </div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->
